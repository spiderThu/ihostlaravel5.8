<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

/*=========================index==================================*/
Route::get('/', 'HomeController@index')->name('index');
Route::get('domain', 'HomeController@domain')->name('domain');
Route::get('hosting', 'HomeController@hosting')->name('hosting');
Route::get('email', 'HomeController@email')->name('email');
Route::get('contact', 'HomeController@contact')->name('contact');
Route::get('project', 'HomeController@project')->name('project');

Auth::routes();

Route::post('send', 'SendController@send')->name('send');
Route::get('searchDomain', 'DomainController@searchDomain')->name('searchDomain');
// Route::get('notes', 'NotesController@index');
// Route::get('pdf', 'NotesController@pdf');

// Route::get('/home', 'HomeController@index')->name('home');
