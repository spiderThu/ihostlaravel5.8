@include('header')

      <!-- slider -->
      <div class="page-name teax-center">
         <script src="js/jssor.slider.min.js" type="text/javascript"></script>
         <script type="text/javascript">
            jssor_1_slider_init = function () {
            
                var jssor_1_options = {
                    $AutoPlay: 1,
                    $SlideDuration: 800,
                    $SlideEasing: $Jease$.$OutQuint,
                    $ArrowNavigatorOptions: {
                        $Class: $JssorArrowNavigator$
                    },
                    $BulletNavigatorOptions: {
                        $Class: $JssorBulletNavigator$
                    }
                };
            
                var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);
            
                /*#region responsive code begin*/
            
                var MAX_WIDTH = 3000;
            
                function ScaleSlider() {
                    var containerElement = jssor_1_slider.$Elmt.parentNode;
                    var containerWidth = containerElement.clientWidth;
            
                    if (containerWidth) {
            
                        var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);
            
                        jssor_1_slider.$ScaleWidth(expectedWidth);
                    } else {
                        window.setTimeout(ScaleSlider, 30);
                    }
                }
            
                ScaleSlider();
            
                $Jssor$.$AddEvent(window, "load", ScaleSlider);
                $Jssor$.$AddEvent(window, "resize", ScaleSlider);
                $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
                /*#endregion responsive code end*/
            };
                
         </script>
         <style>
            /*jssor slider loading skin spin css*/
            .jssorl-009-spin img {
            animation-name: jssorl-009-spin;
            animation-duration: 1.6s;
            animation-iteration-count: infinite;
            animation-timing-function: linear;
            }
            @keyframes jssorl-009-spin {
            from {
            transform: rotate(0deg);
            }
            to {
            transform: rotate(360deg);
            }
            }
            /*jssor slider bullet skin 032 css*/
            .jssorb032 {
            position: absolute;
            }
            .jssorb032 .i {
            position: absolute;
            cursor: pointer;
            }
            .jssorb032 .i .b {
            fill: #fff;
            fill-opacity: 0.7;
            stroke: #000;
            stroke-width: 1200;
            stroke-miterlimit: 10;
            stroke-opacity: 0.25;
            }
            .jssorb032 .i:hover .b {
            fill: #000;
            fill-opacity: .6;
            stroke: #fff;
            stroke-opacity: .35;
            }
            .jssorb032 .iav .b {
            fill: #000;
            fill-opacity: 1;
            stroke: #fff;
            stroke-opacity: .35;
            }
            .jssorb032 .i.idn {
            opacity: .3;
            }
            /*jssor slider arrow skin 051 css*/
            .jssora051 {
            display: block;
            position: absolute;
            cursor: pointer;
            }
            .jssora051 .a {
            fill: none;
            stroke: #fff;
            stroke-width: 360;
            stroke-miterlimit: 10;
            }
            .jssora051:hover {
            opacity: .8;
            }
            .jssora051.jssora051dn {
            opacity: .5;
            }
            .jssora051.jssora051ds {
            opacity: .3;
            pointer-events: none;
            }
         </style>
         <div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:1300px;height:500px;overflow:hidden;visibility:hidden;">
            <!-- Loading Screen -->
            <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
               <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="../svg/loading/static-svg/spin.svg" />
            </div>
            <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:1300px;height:500px;overflow:hidden;">
               <div>
                  <img data-u="image" src="images/slider1.jpg" />
               </div>
               <div>
                  <img data-u="image" src="images/slider2.jpg" />
               </div>
               <div>
                  <img data-u="image" src="images/slider3.jpg" />
               </div>
            </div>
            <!-- Bullet Navigator -->
            <div data-u="navigator" class="jssorb032" style="position:absolute;bottom:12px;right:12px;" data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75">
               <div data-u="prototype" class="i" style="width:16px;height:16px;">
                  <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                     <circle class="b" cx="8000" cy="8000" r="5800"></circle>
                  </svg>
               </div>
            </div>
            <!-- Arrow Navigator -->
            <div data-u="arrowleft" class="jssora051" style="width:65px;height:65px;top:0px;left:25px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
               <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                  <polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
               </svg>
            </div>
            <div data-u="arrowright" class="jssora051" style="width:65px;height:65px;top:0px;right:25px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
               <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                  <polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
               </svg>
            </div>
         </div>
         <script type="text/javascript">
            jssor_1_slider_init();
         </script>
         <!-- #endregion Jssor Slider End -->
      </div>
      <!--//slider -->

      <!-- 3box -->
      <section class="aboutindex py-lg-4 py-md-4 py-sm-4 py-4">
         <div class="container py-lg-5 py-md-5 py-sm-4 py-4">
            <h5 class="own-email">မိမိတို့၏ လုပ်ငန်းအတွက် <span style="color: #60e1bd;">Website </span>တစ်ခုဘာလို့ရှိသင့်သလဲ?</h5>
            <br>
            <div class="row">
               <div class="col-lg-4 col-md-6 col-sm-6 my-3 about-grid-gried">
                  <div class="hovereffect">
                     <div class="number1">
                        <img class="cash" src="images/ladderr.jpg">
                        <div class="overlay">
                           <p class="hovert1">မိတ်ဆွေတို့ရဲ့ စီးပွားရေးလုပ်ငန်းများ   <br/>ယခုထက်ပိုမို တိုးတက်အောင်မြင်ချင်ပါသလား ?</p>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-lg-4 col-md-6 col-sm-6 my-3 about-grid-gried">
                  <div class="hovereffect">
                     <div class="number2" >
                        <img class="website" src="images/web.jpg">   
                        <div class="overlay">
                           <p class="hovert2">အရာရာတိုးတက်ပြောင်းလဲနေတဲ့ ခေတ်ကြီးမှာ   <br/>ကိုယ့်ရဲ့လုပ်ငန်းလူသိများဖို့နဲ့   <br/>
                              အဆင့်အတန်းမြင့်မားဖို့အတွက် <br/>
                              အကောင်းဆုံး Website တစ်ခုတော့ လိုပြီနော်
                           </p>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-lg-4 col-md-6 col-sm-6 my-3 about-grid-gried">
                  <div class="hovereffect">
                     <div class="number3">
                        <img class="gmap" src="images/gmap.jpg">
                        <div class="overlay">
                           <p class="hovert3">လူကြီးမင်းတို့ရဲ့ Website ကို ဝင်ကြည့်ရုံနဲ့ လိပ်စာနှင့်တကွ ကိုယ့်လုပ်ငန်းကိုပါ အလွယ်တကူ သိသွားရုံသာမက  <br/>Google မှာ ရိုက်ရှာလိုက်တာနဲ့  ပြည်တွင်းပြည်ပမှ မည်သူမဆို မသိစေချင်ဘူးလား  ? 
                           </p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!--//3box -->

      <!-- Pricing Tables -->
      <!--price -->
      <div class="packages">
         <h5 class="web">
            Make Your Business More Faster.<br>
            You can choose website packages exactly what you want.<br>
           စိတ်ကြိုက် <span style="color: #60e1bd;"> Web Packages </span> များကို ရွေးချယ်လိုက်ပါ..
         </h5>
         <div class="lm-tabel">
            <div class="row">
               <div class="col-lg-4 col-md-4">
                  <div class="lm-item lm-item-2">
                     <div class="lm-item-top">
                        <span class="lm-item-title lm-underline">Basic</span>
                        <div class="lm-item-price">600,000 Ks</div>
                     </div>
                     <div class="lm-item-body">
                        <!-- <div class="lm-item-desc"><strong>CUSTOM AIR</strong>
                           <p>Air, air, air</p>
                           </div> -->
                        <ul class="lm-item-list">
                           <img src="images/page.png"> &nbsp;  
                           <li>Up to 12 Web Pages</li>
                           <br/> <img src="images/domain.png"> &nbsp;  
                           <li>FREE Domain 1 Year</li>
                           <br/> <img src="images/domain.png"> &nbsp;  
                           <li>FREE Hosting 1 Year</li>
                           <br/><img src="images/email.png"> &nbsp; 
                           <li>FREE 20 Email Accounts</li>
                           <br/> <img src="images/searchh.png"> &nbsp;  
                           <li>SEO Search Engine</li>
                           <br/> <img src="images/phone.png"> &nbsp;  
                           <li>Responsive Design</li>
                           <br/> <img src="images/social.png"> &nbsp;  
                           <li>Social (Fb, Instagram)</li>
                           <br/> <img src="images/contact.png"> &nbsp;  
                           <li>Contact Form</li>
                           <br/> <img src="images/location.png"> &nbsp; 
                           <li>Location Map</li>
                           <br/> <img src="images/maintain.png"> &nbsp;  
                           <li>FREE 1 Year Maintenance</li>
                        </ul>
                        <div class="lm-item-link">
                           <a href="#">
                              <svg width="152" height="52" xmlns="http://www.w3.org/2000/svg">
                                 <defs>
                                    <linearGradient id="lm-gr-2" x1="0" y1="0" x2="100%" y2="100%">
                                       <stop offset="0%" stop-color="#7E052D"></stop>
                                       <stop offset="100%" stop-color="#F6266B"></stop>
                                    </linearGradient>
                                 </defs>
                                 <rect x="1" y="1" width="150" height="50" rx="25" fill="#fff" stroke="red" stroke-width="1"></rect>
                                 <a href="tel:+959795938304"><text x="21%" y="62%" fill="red">CHOOSE</text></a>
                              </svg>
                           </a>
                        </div>
                     </div>
                     <div class="lm-item-bottom"></div>
                  </div>
               </div>
               <div class="col-lg-4 col-md-4">
                  <div class="lm-item lm-item-1">
                     <div class="lm-item-top">
                        <span class="lm-item-title lm-underline">STANDARD</span>
                        <div class="lm-item-price">1,000,000 Ks</div>
                     </div>
                     <div class="lm-item-body">
                        <!-- <div class="lm-item-desc"><strong>CUSTOM AIR</strong>
                           <p>Air, air, air</p>
                           </div> -->
                        <ul class="lm-item-list">
                           <img src="images/page.png"> &nbsp;  
                           <li>
                              Up to 25 Web Pages<!-- <i class="icon-cancel"></i> -->
                           </li>
                           <br/> <img src="images/domain.png"> &nbsp;  
                           <li>
                              FREE Domain 1 Year<!-- <i class="icon-ok"></i> -->
                           </li>
                           <br/> <img src="images/domain.png"> &nbsp;  
                           <li>
                              FREE Hosting 1 Year<!-- <i class="icon-ok"></i> -->
                           </li>
                           <br/><img src="images/email.png"> &nbsp; 
                           <li>
                              FREE 200 Email Accounts<!-- <i class="icon-cancel"></i> -->
                           </li>
                           <br/> <img src="images/searchh.png"> &nbsp;  
                           <li>
                              SEO Search Engine<!-- <i class="icon-cancel"></i> -->
                           </li>
                           <br/> <img src="images/phone.png"> &nbsp;  
                           <li>
                              Responsive Design<!-- <i class="icon-cancel"></i> -->
                           </li>
                           <br/> <img src="images/social.png"> &nbsp;  
                           <li>Social (Fb, Instagram)</li>
                           <br/> <img src="images/contact.png"> &nbsp;  
                           <li>Contact Form</li>
                           <br/> <img src="images/location.png"> &nbsp; 
                           <li>Location Map</li>
                           <br/> <img src="images/maintain.png"> &nbsp;  
                           <li>FREE 1 Year Maintenance</li>
                        </ul>
                        </ul>
                        <div class="lm-item-link">
                           <a href="#">
                              <svg width="152" height="52" xmlns="http://www.w3.org/2000/svg">
                                 <defs>
                                    <linearGradient id="lm-gr-1" x1="0" y1="0" x2="100%" y2="100%">
                                       <stop offset="0%" stop-color="#241326"></stop>
                                       <stop offset="100%" stop-color="#85468C"></stop>
                                    </linearGradient>
                                 </defs>
                                 <rect x="1" y="1" width="150" height="50" rx="25" fill="#fff" stroke="blue" stroke-width="1"></rect>
                                 <a href="tel:+959795938304"><text x="21%" y="65%" fill="blue">CHOOSE</text></a>
                              </svg>
                           </a>
                        </div>
                     </div>
                     <div class="lm-item-bottom"></div>
                  </div>
               </div>
               <div class="col-lg-4 col-md-4">
                  <div class="lm-item lm-item-3">
                     <div class="lm-item-top">
                        <span class="lm-item-title lm-underline">PREMIUM</span>
                        <div class="lm-item-price">1,800,000 Ks</div>
                     </div>
                     <div class="lm-item-body">
                        <!--  <div class="lm-item-desc"><strong>CUSTOM AIR</strong>
                           <p>Air, air, air</p>
                           </div> -->
                        <ul class="lm-item-list">
                           <img src="images/page.png"> &nbsp;  
                           <li>
                              Unlimited Web Pages<!-- <i class="icon-cancel"></i> -->
                           </li>
                           <br/> <img src="images/domain.png"> &nbsp;  
                           <li>
                              FREE Domain 1 Year<!-- <i class="icon-ok"></i> -->
                           </li>
                           <br/> <img src="images/domain.png"> &nbsp;  
                           <li>
                              FREE Hosting 1 Year<!-- <i class="icon-ok"></i> -->
                           </li>
                           <br/><img src="images/email.png"> &nbsp; 
                           <li>
                              Unlimited Email Accounts<!-- <i class="icon-cancel"></i> -->
                           </li>
                           <br/> <img src="images/searchh.png"> &nbsp;  
                           <li>
                              SEO Search Engine<!-- <i class="icon-cancel"></i> -->
                           </li>
                           <br/> <img src="images/phone.png"> &nbsp;  
                           <li>
                              Responsive Design<!-- <i class="icon-cancel"></i> -->
                           </li>
                           <br/> <img src="images/social.png"> &nbsp;  
                           <li>Social (Fb, Instagram)</li>
                           <br/> <img src="images/contact.png"> &nbsp;  
                           <li>Contact Form</li>
                           <br/> <img src="images/location.png"> &nbsp; 
                           <li>Location Map</li>
                           <br/> <img src="images/dollar.png"> &nbsp; 
                           <li> Payment Inegration</li>
                           <br/> <img src="images/maintain.png"> &nbsp;  
                           <li>FREE 1 Year Maintenance</li>
                        </ul>
                        </ul>
                        <div class="lm-item-link">
                           <a href="#">
                              <svg width="152" height="52" xmlns="http://www.w3.org/2000/svg">
                                 <defs>
                                    <linearGradient id="lm-gr" x1="0" y1="0" x2="100%" y2="100%">
                                       <stop offset="0%" stop-color="#1A466F"></stop>
                                       <stop offset="100%" stop-color="#4D93D5"></stop>
                                    </linearGradient>
                                 </defs>
                                 <rect x="1" y="1" width="150" height="50" rx="25" fill="#fff" stroke="orange" stroke-width="1"></rect>
                                 <a href="tel:+959795938304"><text x="21%" y="62%" fill="orange">CHOOSE</text></a>
                              </svg>
                           </a>
                        </div>
                     </div>
                     <div class="lm-item-bottom"></div>
                  </div>
               </div>
            </div>
            <script src="js/jquery-2.1.4.min.js"></script>
            <script>
               (function () {
                   $(document).ready(function () {
                       var arrItems;
                       arrItems = $('.lm-item');
                       return arrItems.hover(function () {
                           return arrItems.addClass('blur');
                       }, function () {
                           return arrItems.removeClass('blur');
                       });
                   });
               
               }).call(this);
            </script>
         </div>
      </div>
      <!-- Pricing Tables -->
      <!--//price -->

      <!-- Responsive Bagan -->
      <section class="counterindex py-lg-4 py-md-3 py-sm-3 py-3">
         <div class="container py-lg-5 py-md-5 py-sm-4 py-3">
            <div class="row">
               <div class="col-lg-12" style="margin-top: -3em;">
                  <img src="images/responsive.png" class="bagan">
               </div>
            </div>
         </div>
      </section>
      <!--//Responsive Bagan -->

      <!-- weprovide -->
      <section class="service py-lg-4 py-md-3 py-sm-3 py-3">
         <div class="container py-lg-5 py-md-5 py-sm-4 py-3">
            <h3 class="title mb-3">What We Provide </h3>
            <div class="row">
               <div class="col-lg-6 col-md-7">
                  <div class="service-grid-wthree mt-lg-5 mt-md-4 my-3">
                     <div class="row mt-lg-4 mt-md-4 mt-sm-3 mt-2">
                        <div class="col-lg-2 col-md-3 col-sm-2 service-jst-icon">
                           <div class="service-icon text-center ">
                              <!-- <span class="fa fa-money" aria-hidden="true"></span> -->
                              <img src="images/Invoice-icon.png" style="padding: 3px;">
                           </div>
                        </div>
                        <div class="ser-sevice-grid col-lg-10 col-md-9 col-sm-10">
                           <!-- <h4>Payment Services</h4> -->
                           <p class="mt-2">Credit/Debit Card (သို့) PayPal အသုံးပြုပြီး တိုက်ရိုက်ဝယ်ယူနိုင်ပါသည်။ ငွေသားဖြင့်ပေချေမည်ဆိုပါက ရုံးသို့လာရောက်ဝယ်ယူနိုင်ပါသည်။
                              <br/> နယ်မှ ဝယ်ယူသူများလည်း Bank Account သို့ ငွေလွဲဝယ်ယူနိုင်ပါသည်။
                           </p>
                        </div>
                     </div>
                  </div>
                  <div class="service-grid-wthree mt-lg-5 mt-md-4 my-3">
                     <div class="row mt-lg-4 mt-md-4 mt-sm-3 mt-2">
                        <div class="col-lg-2 col-md-3 col-sm-2">
                           <div class="service-icon text-center ">
                              <!-- <span class="fa fa-commenting" aria-hidden="true"></span> -->
                              <img src="images/help.png" style="padding: 3px;">
                           </div>
                        </div>
                        <div class="ser-sevice-grid col-lg-10 col-md-9 col-sm-10">
                           <!--  <h4>Expert Advisers</h4> -->
                           <p class="mt-2">မိတ်ဆွေရဲ့ Website ကို Hosting လုပ်ပြီးသွားတဲ့အချိန်မှာလဲ
                              <br/> မိတ်ဆွေတို့ဘက်မှ လိုအပ်သော ဝန်ဆောင်မှုကို ကျွန်ုပ်တို့ဘက်မှ
                              <br/> အပြည့်အဝထောက်ပံ့ပေးမည် ဖြစ်ပါသည်။
                           </p>
                        </div>
                     </div>
                  </div>
                  <div class="service-grid-wthree mt-lg-5 mt-md-4 my-3">
                     <div class="row mt-lg-4 mt-md-4 mt-sm-3 mt-2">
                        <div class="col-lg-2 col-md-3 col-sm-2">
                           <div class="service-icon text-center ">
                              <!-- <span class="fa fa-clock-o" aria-hidden="true"></span> -->
                              <img src="images/support.png" style="padding: 3px;">
                           </div>
                        </div>
                        <div class="ser-sevice-grid col-lg-10 col-md-9 col-sm-10">
                           <!--  <h4>24/7 Customer Support</h4> -->
                           <p class="mt-2">
                             သိလိုသမျှ အရာအားလုံးကို ဆက်သွယ်မေးမြန်းနိုင်ပါသည်။
                              <br/> iHost က မိတ်ဆွေတို့အတွက် (24) နာရီ ဖွင့်လှစ်ထားပါသည်။
                              <br/> ရုံးဖုန်းနံပါတ် - 09977870421 , 09977870422
                           </p>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-lg-6 col-md-5 text-center">
                  <img src="images/service.png" alt="news image" class="img-flui">
               </div>
            </div>
         </div>
      </section>
      <!--//weprovide -->

      <!-- service -->
      <div id="support">
         မိတ်ဆွေတို့ရဲ့ လုပ်ငန်းများ Internet ပေါ်တွင် ပိုမို အောင်မြင်စေရန်အတွက် iHost မှ တာဝန်ယူ ဆောင်ရွက်ပေးပါရစေ။
         <br/> iHost Helps Your Business More Better......
      </div>
      <!--//service -->

@include('footer')      

