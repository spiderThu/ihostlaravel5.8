<!DOCTYPE html>
<html lang="en">
   <head>
      <title>iHost : Web Hosting & Domain</title>
      <!--meta tags -->
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content="ihost,iHost,domain,hosting,web hosting,email,myanmar hosting,spider" />
      <LINK rel="SHORTCUT ICON" href="images/ishortcut.png">
      <link href="css/main.css" rel="stylesheet" />
      <script src="js/bootstrap.min.js"></script>
      <script>
         addEventListener("load", function() {
             setTimeout(hideURLbar, 0);
         }, false);
         
         function hideURLbar() {
             window.scrollTo(0, 1);
         }
      </script>
      <!--booststrap-->
      <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
      <!--//booststrap end-->
      <!-- font-awesome icons -->
      <link href="css/font-awesome.min.css" rel="stylesheet">
      <!-- //font-awesome icons -->
      <!--stylesheets-->
      <link href="css/style.css" rel='stylesheet' type='text/css' media="all">
      <link href="css/responsive.css" rel='stylesheet' type='text/css' media="all">
      <!--//stylesheets-->
      <link href="//fonts.googleapis.com/css?family=Cabin:400,500,600,700" rel="stylesheet">
      <link href="//fonts.googleapis.com/css?family=Roboto:300,400,500" rel="stylesheet">
      <script src = "https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
      <script>
         $(document).ready(function(myFunction) {
           $("#btns").click(function() {
              //window.location = "#tabMenu";
              $('html, body').animate({ scrollTop: $('#tabMenu').offset().top }, 'slow');
           });
         });
      </script>
      <!-- <script>
         function myFunction() {
           alert("Page is loaded");
         }
         </script> -->
      <!--  <script>
         document.getElementById("#tabMenu").onload = function() {myFunction()};
         
         function myFunction() {
           window.location = "#tabMenu";
         }
         </script> -->
   </head>
   <body>
      <div  id="home">
         <!-- header -->
         <div class="headder-top d-lg-flex justify-content-between align-items-center py-3 px-sm-3">
            <!-- logo -->
            <div id="logo">
               <h1>
                  <a href="{{ route('index') }}">iHost</a>
               </h1>
            </div>
            <!-- //logo -->
            <!-- nav -->
            <nav>
               <label for="drop" class="toggle">Menu</label>
               <input type="checkbox" id="drop" />
               <ul class="menu">
                  <li>
                     <a href="{{ route('index') }}">Web</a>
                  </li>
                  <li>
                     <a href="{{ route('domain') }}" class="active">Domain</a>
                  </li>
                  <li>
                     <a href="{{ route('hosting') }}">Hosting</a>
                  </li>
                  <li>
                     <a href="{{ route('email') }}">Email</a>
                  </li>
                  <a href="{{ route('contact') }}">Contact</a>
                  </li>
                  <li>
                     <a href="{{ route('project') }}">Our Projects</a>
                  </li>
               </ul>
            </nav>
            <!-- //nav -->
            <div class="d-flex mt-lg-1 mt-sm-2 mt-3 justify-content-center">
               <div class="search-w3layouts">
                  <!-- <span class="fa fa-users" aria-hidden="true"> -->
                  <button style="font-size:17px;padding: 8px 1px;">👩🏻‍💻 LOGIN</button>
                  </span>
                  <!-- modal form -->
                  <div class="popup">
                     <div class="content">
                        <span class="closebtn">&#9932;</span>
                        <div class="title">
                           <h1>Admin Login</h1>
                        </div>
                        <!-- <img src="https://webdevtrick.com/wp-content/uploads/logo-fb-1.png" alt="Car"> -->
                        <div class="subscribe">
                           <!-- <h1>Subscribe To Get The Notification Of Latest <span>POSTS</span></h1> -->
                           <form>
                              <input type="email" placeholder="Email Address">
                              <input type="password" placeholder="Password">
                              <input type="submit" value="Login">
                           </form>
                        </div>
                     </div>
                  </div>
                  <script src='js/jquery.min.js'></script>
                  <script src="js/modal.js"></script>
                  <!-- Modal login form -->
               </div>
            </div>
         </div>
         <div class="page-name teax-center">
            <!-- <h5>မြန်မာပြည်တွင်စျေးသက်သာသော <span style="color: #60e1bd;">Domain</span> ဝန်ဆောင်မှု</h5> -->
         </div>
      </div>
      </div>
      <!-- //header -->
      <!-- search domain results -->
      <div class="for-domain" style="background-color:#f9f9f9;">
         <div class="container col-lg-12 col-md-12 col-sm-12" >
            <div class="row" style="background-color:#f9f9f9;">
               <div class="col-lg-7 col-md-7 col-sm-7">
                  <div class="many-domains">  
                     <?php  
                        if(isset($already)){
                           echo "Sorry! your domain is unavailable.Already Register! 😟";
                        }
                        
                        if(isset($success)){
                           echo "Your domain is available.😊 You can register it!";
                        }
                        ?>
                  </div>
                  <div class="name-for-domains">
                     <p class="icon-border">
                        <button style="border:none;background-color: transparent;">
                        <?php  
                           if(isset($already)){
                              echo "✖️";
                           }
                           
                           if(isset($success)){
                              echo "✔️";
                           }
                           ?>
                        </button>
                        <?php 
                           //$domain = str_replace("",".com",$domain);
                           $checkd = $domain;
                           if (strpos($checkd, '.') !== false) { //if .has
                               echo $checkd;
                           }
                           else{
                              echo $checkd.".com";
                           }
                           ?>
                        &nbsp; &nbsp;<button class="button-for-domain"> 
                        <?php  
                           if(isset($already)){
                              echo "";
                           }
                           
                           if(isset($success)){
                              echo "POPULAR";
                           }
                           ?>
                        </button> 
                     </p>
                     <p class="prices">
                        <?php  
                           if(isset($already)){
                              echo "<a href='http://localhost/iHostLaravel5.8/domain' style='color: #60e1bd;'>Search Again◀️</a>";
                           }
                           
                           if(isset($success)){
                              if(strpos($domain, '.com') !== false){
                                  echo "MMK 12,000";
                              }
                           }
                           ?>
                     </p>
                  </div>
                  <!-- domain names recommended  -->
                  <div class="domain-name-recommended">Recommended Domains</div>
                  <div class="sample-domains">
                     <p class="icon-border">
                        <button style="border:none;background-color: transparent;">
                        ✅️                        </button>
                        <?php 
                           $chkdom = substr($domain, 0, strpos($domain, '.'));
                           if($chkdom){
                              echo $chkdom.".com.mm";
                           }  
                           else{
                              echo $domain.".com.mm";
                           }
                           ?>
                     </p>
                     <p class="prices"> MMK 70,000 &nbsp;
                        <a href="tel:+959795938304"><button class="get-now"> Choose</button></a>
                     </p>
                  </div>
                  <div class="sample-domains1">
                     <p class="icon-border">
                        <button style="border:none;background-color: transparent;">
                        ✅️                        </button>
                        <?php 
                           $chkdom = substr($domain, 0, strpos($domain, '.'));
                           if($chkdom){
                              echo $chkdom.".net";
                           }  
                           else{
                              echo $domain.".net";
                           }
                           ?>
                     </p>
                     <p class="prices"> MMK 17,500 &nbsp;
                        <a href="tel:+959795938304"><button class="get-now"> Choose</button></a>
                     </p>
                  </div>
                  <div class="sample-domains">
                     <p class="icon-border">
                        <button style="border:none;background-color: transparent;">
                        ✅️                        </button>
                        <?php 
                           $chkdom = substr($domain, 0, strpos($domain, '.'));
                           if($chkdom){
                              echo $chkdom.".online";
                           }  
                           else{
                              echo $domain.".online";
                           }
                           ?>
                     </p>
                     <p class="prices">MMK 3,000 &nbsp;
                        <a href="tel:+959795938304"><button class="get-now"> Choose</button></a>
                     </p>
                  </div>
                  <div class="sample-domains1">
                     <p class="icon-border">
                        <button style="border:none;background-color: transparent;">
                        ✅️                        </button>
                        <?php 
                           $chkdom = substr($domain, 0, strpos($domain, '.'));
                           if($chkdom){
                              echo $chkdom.".info";
                           }  
                           else{
                              echo $domain.".info";
                           }
                           ?>
                     </p>
                     <p class="prices">MMK 5,000 &nbsp;
                        <a href="tel:+959795938304"><button class="get-now"> Choose</button></a>
                     </p>
                  </div>
                  <div class="sample-domains">
                     <p class="icon-border">
                        <button style="border:none;background-color: transparent;">
                        ✅️                        </button>
                        <?php 
                           $chkdom = substr($domain, 0, strpos($domain, '.'));
                           if($chkdom){
                              echo $chkdom.".org";
                           }  
                           else{
                              echo $domain.".org";
                           }
                           ?>
                     </p>
                     <p class="prices">MMK 17,500 &nbsp;
                        <a href="tel:+959795938304"><button class="get-now"> Choose</button></a>
                     </p>
                  </div>
                  <div class="sample-domains1">
                     <p class="icon-border">
                        <button style="border:none;background-color: transparent;">
                        ✅️                        </button>
                        <?php 
                           $chkdom = substr($domain, 0, strpos($domain, '.'));
                           if($chkdom){
                              echo $chkdom.".inc";
                           }  
                           else{
                              echo $domain.".inc";
                           }
                           ?>
                     </p>
                     <p class="prices">MMK 1,000,000 &nbsp;
                        <a href="tel:+959795938304"><button class="get-now"> Choose</button></a>
                     </p>
                  </div>
                  <div class="sample-domains">
                     <p class="icon-border">
                        <button style="border:none;background-color: transparent;">
                        ✅️                        </button>
                        <?php 
                           $chkdom = substr($domain, 0, strpos($domain, '.'));
                           if($chkdom){
                              echo $chkdom.".shop";
                           }  
                           else{
                              echo $domain.".shop";
                           }
                           ?>
                     </p>
                     <p class="prices">MMK 17,500 &nbsp;
                        <a href="tel:+959795938304"><button class="get-now"> Choose</button></a>
                     </p>
                  </div>
                  <div class="sample-domains1">
                     <p class="icon-border">
                        <button style="border:none;background-color: transparent;">
                        ✅️                        </button>
                        <?php 
                           $chkdom = substr($domain, 0, strpos($domain, '.'));
                           if($chkdom){
                              echo $chkdom.".website";
                           }  
                           else{
                              echo $domain.".website";
                           }
                           ?>
                     </p>
                     <p class="prices">MMK 3,000 &nbsp;
                        <a href="tel:+959795938304"><button class="get-now"> Choose</button></a>
                     </p>
                  </div>
                  <div class="sample-domains">
                     <p class="icon-border">
                        <button style="border:none;background-color: transparent;">
                        ✅️                        </button>
                        <?php 
                           $chkdom = substr($domain, 0, strpos($domain, '.'));
                           if($chkdom){
                              echo $chkdom.".la";
                           }  
                           else{
                              echo $domain.".la";
                           }
                           ?>
                     </p>
                     <p class="prices">MMK 45,000 &nbsp;
                        <a href="tel:+959795938304"><button class="get-now"> Choose</button></a>
                     </p>
                  </div>
                  <div class="sample-domains1">
                     <p class="icon-border">
                        <button style="border:none;background-color: transparent;">
                        ✅️                        </button>
                        <?php 
                           $chkdom = substr($domain, 0, strpos($domain, '.'));
                           if($chkdom){
                              echo $chkdom.".email";
                           }  
                           else{
                              echo $domain.".email";
                           }
                           ?>
                     </p>
                     <p class="prices">MMK 5,000 &nbsp;
                        <a href="tel:+959795938304"><button class="get-now"> Choose</button></a>
                     </p>
                  </div>
                  <div class="sample-domains">
                     <p class="icon-border">
                        <button style="border:none;background-color: transparent;">
                        ✅️                        </button>
                        <?php 
                           $chkdom = substr($domain, 0, strpos($domain, '.'));
                           if($chkdom){
                              echo $chkdom.".site";
                           }  
                           else{
                              echo $domain.".site";
                           }
                           ?>
                     </p>
                     <p class="prices">MMK 3,000 &nbsp;
                        <a href="tel:+959795938304"><button class="get-now"> Choose</button></a>
                     </p>
                  </div>
                  <div class="sample-domains1">
                     <p class="icon-border">
                        <button style="border:none;background-color: transparent;">
                        ✅️                        </button>
                        <?php 
                           $chkdom = substr($domain, 0, strpos($domain, '.'));
                           if($chkdom){
                              echo $chkdom.".me";
                           }  
                           else{
                              echo $domain.".me";
                           }
                           ?>
                     </p>
                     <p class="prices">MMK 10,000 &nbsp;
                        <a href="tel:+959795938304"><button class="get-now"> Choose</button></a>
                     </p>
                  </div>
                  <div class="sample-domains">
                     <p class="icon-border">
                        <button style="border:none;background-color: transparent;">
                        ✅️                        </button>
                        <?php 
                           $chkdom = substr($domain, 0, strpos($domain, '.'));
                           if($chkdom){
                              echo $chkdom.".cloud";
                           }  
                           else{
                              echo $domain.".cloud";
                           }
                           ?>
                     </p>
                     <p class="prices">MMK 7,000 &nbsp;
                        <a href="tel:+959795938304"><button class="get-now"> Choose</button></a>
                     </p>
                  </div>
                  <div class="sample-domains1">
                     <p class="icon-border">
                        <button style="border:none;background-color: transparent;">
                        ✅️                        </button>
                        <?php 
                           $chkdom = substr($domain, 0, strpos($domain, '.'));
                           if($chkdom){
                              echo $chkdom.".xyz";
                           }  
                           else{
                              echo $domain.".xyz";
                           }
                           ?>
                     </p>
                     <p class="prices">MMK 3,000 &nbsp;
                        <a href="tel:+959795938304"><button class="get-now"> Choose</button></a>
                     </p>
                  </div>
                  <div class="sample-domains">
                     <p class="icon-border">
                        <button style="border:none;background-color: transparent;">
                        ✅️                        </button>
                        <?php 
                           $chkdom = substr($domain, 0, strpos($domain, '.'));
                           if($chkdom){
                              echo $chkdom.".biz";
                           }  
                           else{
                              echo $domain.".biz";
                           }
                           ?>
                     </p>
                     <p class="prices">MMK 5,000 &nbsp;
                        <a href="tel:+959795938304"><button class="get-now"> Choose</button></a>
                     </p>
                  </div>
                  <div class="sample-domains1">
                     <p class="icon-border">
                        <button style="border:none;background-color: transparent;">
                        ✅️                        </button>
                        <?php 
                           $chkdom = substr($domain, 0, strpos($domain, '.'));
                           if($chkdom){
                              echo $chkdom.".club";
                           }  
                           else{
                              echo $domain.".club";
                           }
                           ?>
                     </p>
                     <p class="prices">MMK 3,000 &nbsp;
                        <a href="tel:+959795938304"><button class="get-now"> Choose</button></a>
                     </p>
                  </div>
                  <div class="sample-domains">
                     <p class="icon-border">
                        <button style="border:none;background-color: transparent;">
                        ✅️                        </button>
                        <?php 
                           $chkdom = substr($domain, 0, strpos($domain, '.'));
                           if($chkdom){
                              echo $chkdom.".fun";
                           }  
                           else{
                              echo $domain.".fun";
                           }
                           ?>
                     </p>
                     <p class="prices">MMK 3,000 &nbsp;
                        <a href="tel:+959795938304"><button class="get-now"> Choose</button></a>
                     </p>
                  </div>
                  <div class="sample-domains1">
                     <p class="icon-border">
                        <button style="border:none;background-color: transparent;">
                        ✅️                        </button>
                        <?php 
                           $chkdom = substr($domain, 0, strpos($domain, '.'));
                           if($chkdom){
                              echo $chkdom.".services";
                           }  
                           else{
                              echo $domain.".services";
                           }
                           ?>
                     </p>
                     <p class="prices">MMK 35,000 &nbsp;
                        <a href="tel:+959795938304"><button class="get-now"> Choose</button></a>
                     </p>
                  </div>
                  <div class="sample-domains">
                     <p class="icon-border">
                        <button style="border:none;background-color: transparent;">
                        ✅️                        </button>
                        <?php 
                           $chkdom = substr($domain, 0, strpos($domain, '.'));
                           if($chkdom){
                              echo $chkdom.".work";
                           }  
                           else{
                              echo $domain.".work";
                           }
                           ?>
                     </p>
                     <p class="prices">MMK 3,000 &nbsp;
                        <a href="tel:+959795938304"><button class="get-now"> Choose</button></a>
                     </p>
                  </div>
                  <div class="sample-domains1">
                     <p class="icon-border">
                        <button style="border:none;background-color: transparent;">
                        ✅️                        </button>
                        <?php 
                           $chkdom = substr($domain, 0, strpos($domain, '.'));
                           if($chkdom){
                              echo $chkdom.".asia";
                           }  
                           else{
                              echo $domain.".asia";
                           }
                           ?>
                     </p>
                     <p class="prices">MMK 7,000 &nbsp;
                        <a href="tel:+959795938304"><button class="get-now"> Choose</button></a>
                     </p>
                  </div>
                  <div class="sample-domains">
                     <p class="icon-border">
                        <button style="border:none;background-color: transparent;">
                        ✅️                        </button>
                        <?php 
                           $chkdom = substr($domain, 0, strpos($domain, '.'));
                           if($chkdom){
                              echo $chkdom.".pw";
                           }  
                           else{
                              echo $domain.".pw";
                           }
                           ?>
                     </p>
                     <p class="prices">MMK 3,000 &nbsp;
                        <a href="tel:+959795938304"><button class="get-now"> Choose</button></a>
                     </p>
                  </div>
                  <div class="sample-domains1">
                     <p class="icon-border">
                        <button style="border:none;background-color: transparent;">
                        ✅️                        </button>
                        <?php 
                           $chkdom = substr($domain, 0, strpos($domain, '.'));
                           if($chkdom){
                              echo $chkdom.".tech";
                           }  
                           else{
                              echo $domain.".tech";
                           }
                           ?>
                     </p>
                     <p class="prices">MMK 7,000 &nbsp;
                        <a href="tel:+959795938304"><button class="get-now"> Choose</button></a>
                     </p>
                  </div>
                  <div class="sample-domains">
                     <p class="icon-border">
                        <button style="border:none;background-color: transparent;">
                        ✅️                        </button>
                        <?php 
                           $chkdom = substr($domain, 0, strpos($domain, '.'));
                           if($chkdom){
                              echo $chkdom.".top";
                           }  
                           else{
                              echo $domain.".top";
                           }
                           ?>
                     </p>
                     <p class="prices">MMK 3,000 &nbsp;
                        <a href="tel:+959795938304"><button class="get-now"> Choose</button></a>
                     </p>
                  </div>
                  <div class="sample-domains1">
                     <p class="icon-border">
                        <button style="border:none;background-color: transparent;">
                        ✅️                        </button>
                        <?php 
                           $chkdom = substr($domain, 0, strpos($domain, '.'));
                           if($chkdom){
                              echo $chkdom.".company";
                           }  
                           else{
                              echo $domain.".company";
                           }
                           ?>
                     </p>
                     <p class="prices">MMK 10,000 &nbsp;
                        <a href="tel:+959795938304"><button class="get-now"> Choose</button></a>
                     </p>
                  </div>
                  <div class="sample-domains">
                     <p class="icon-border">
                        <button style="border:none;background-color: transparent;">
                        ✅️                        </button>
                        <?php 
                           $chkdom = substr($domain, 0, strpos($domain, '.'));
                           if($chkdom){
                              echo $chkdom.".news";
                           }  
                           else{
                              echo $domain.".news";
                           }
                           ?>
                     </p>
                     <p class="prices">MMK 15,000 &nbsp;
                        <a href="tel:+959795938304"><button class="get-now"> Choose</button></a>
                     </p>
                  </div>
                  <div class="sample-domains1">
                     <p class="icon-border">
                        <button style="border:none;background-color: transparent;">
                        ✅️                        </button>
                        <?php 
                           $chkdom = substr($domain, 0, strpos($domain, '.'));
                           if($chkdom){
                              echo $chkdom.".org.mm";
                           }  
                           else{
                              echo $domain.".org.mm";
                           }
                           ?>
                     </p>
                     <p class="prices">MMK 70,000 &nbsp;
                        <a href="tel:+959795938304"><button class="get-now"> Choose</button></a>
                     </p>
                  </div>
                  <div class="sample-domains">
                     <p class="icon-border">
                        <button style="border:none;background-color: transparent;">
                        ✅️                        </button>
                        <?php 
                           $chkdom = substr($domain, 0, strpos($domain, '.'));
                           if($chkdom){
                              echo $chkdom.".biz.mm";
                           }  
                           else{
                              echo $domain.".biz.mm";
                           }
                           ?>
                     </p>
                     <p class="prices">MMK 70,000 &nbsp;
                        <a href="tel:+959795938304"><button class="get-now"> Choose</button></a>
                     </p>
                  </div>
                  <div class="sample-domains1">
                     <p class="icon-border">
                        <button style="border:none;background-color: transparent;">
                        ✅️                        </button>
                        <?php 
                           $chkdom = substr($domain, 0, strpos($domain, '.'));
                           if($chkdom){
                              echo $chkdom.".co";
                           }  
                           else{
                              echo $domain.".co";
                           }
                           ?>
                     </p>
                     <p class="prices">MMK 20,000 &nbsp;
                        <a href="tel:+959795938304"><button class="get-now"> Choose</button></a>
                     </p>
                  </div>
                  <div class="sample-domains">
                     <p class="icon-border">
                        <button style="border:none;background-color: transparent;">
                        ✅️                        </button>
                        <?php 
                           $chkdom = substr($domain, 0, strpos($domain, '.'));
                           if($chkdom){
                              echo $chkdom.".net.mm";
                           }  
                           else{
                              echo $domain.".net.mm";
                           }
                           ?>
                     </p>
                     <p class="prices">MMK 70,000 &nbsp;
                        <a href="tel:+959795938304"><button class="get-now"> Choose</button></a>
                     </p>
                  </div>
                  <div class="sample-domains1">
                     <p class="icon-border">
                        <button style="border:none;background-color: transparent;">
                        ✅️                        </button>
                        <?php 
                           $chkdom = substr($domain, 0, strpos($domain, '.'));
                           if($chkdom){
                              echo $chkdom.".per.mm";
                           }  
                           else{
                              echo $domain.".per.mm";
                           }
                           ?>
                     </p>
                     <p class="prices">MMK 70,000 &nbsp;
                        <a href="tel:+959795938304"><button class="get-now"> Choose</button></a>
                     </p>
                  </div>
                  <div class="sample-domains">
                     <p class="icon-border">
                        <button style="border:none;background-color: transparent;">
                        ✅️                        </button>
                        <?php 
                           $chkdom = substr($domain, 0, strpos($domain, '.'));
                           if($chkdom){
                              echo $chkdom.".pro";
                           }  
                           else{
                              echo $domain.".pro";
                           }
                           ?>
                     </p>
                     <p class="prices">MMK 7,000 &nbsp;
                        <a href="tel:+959795938304"><button class="get-now"> Choose</button></a>
                     </p>
                  </div>
                  <div class="sample-domains1">
                     <p class="icon-border">
                        <button style="border:none;background-color: transparent;">
                        ✅️                        </button>
                        <?php 
                           $chkdom = substr($domain, 0, strpos($domain, '.'));
                           if($chkdom){
                              echo $chkdom.".mobi";
                           }  
                           else{
                              echo $domain.".mobi";
                           }
                           ?>
                     </p>
                     <p class="prices">MMK 7,000 &nbsp;
                        <a href="tel:+959795938304"><button class="get-now"> Choose</button></a>
                     </p>
                  </div>
                  <div class="sample-domains">
                     <p class="icon-border">
                        <button style="border:none;background-color: transparent;">
                        ✅️                        </button>
                        <?php 
                           $chkdom = substr($domain, 0, strpos($domain, '.'));
                           if($chkdom){
                              echo $chkdom.".ltd";
                           }  
                           else{
                              echo $domain.".ltd";
                           }
                           ?>
                     </p>
                     <p class="prices">MMK 10,000 &nbsp;
                        <a href="tel:+959795938304"><button class="get-now"> Choose</button></a>
                     </p>
                  </div>
               </div>
               <!--// domain names recommended   -->
               <div class="col-lg-5 col-md-5 col-sm-5">
                  <div class="domain-order-cart">
                     <div class="order">   🛒 &nbsp; အမှာစာ</div>
                     <div class="names">
                        <p class="name-domain"> <?php  
                           if(isset($already)){
                              echo "✖️";
                           }
                           
                           if(isset($success)){
                              echo "✔️";
                           }
                           ?>
                           {{$domain}}
                        </p>
                        <br>
                        <p class="year">
                           <?php  
                              if(isset($already)){
                                 echo "";
                              }
                              
                              if(isset($success)){
                                 echo "1 Year";
                              }
                              ?>
                        </p>
                        <p class="price">
                           <?php  
                              if(isset($already)){
                                 echo "";
                              }
                              
                              if(isset($success)){
                                 if(strpos($domain, '.com') !== false){
                                  echo "MMK 12,000";
                                 }
                              }
                              ?>
                        </p>
                        <p class="buy-domains-button"> 
                           <?php  
                              if(isset($already)){
                                 echo "";
                              }
                              
                              if(isset($success)){
                                 echo "<a href='tel:+959795938304'><button class='buy-domains'>Buy Domain</button></a>";
                              }
                              ?>                 
                        </p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--//search domain results -->
   </body>
   @include('footer')

