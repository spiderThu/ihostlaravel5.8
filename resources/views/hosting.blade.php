<!DOCTYPE html>
<html lang="en">
   <head>
      <title>iHost : Web Hosting & Domain</title>
      <!--meta tags -->
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content="ihost,iHost,domain,hosting,web hosting,email,myanmar hosting,spider"/>
      <LINK rel="SHORTCUT ICON" href="images/ishortcut.png">
      <script>
         addEventListener("load", function () {
             setTimeout(hideURLbar, 0);
         }, false);
         
         function hideURLbar() {
             window.scrollTo(0, 1);
         }
      </script>
      <!--booststrap-->
      <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
      <!--//booststrap end-->
      <!-- font-awesome icons -->
      <link href="css/font-awesome.min.css" rel="stylesheet">
      <!-- //font-awesome icons -->
      <!--stylesheets-->
      <link href="css/style.css" rel='stylesheet' type='text/css' media="all">
      <link href="css/responsive.css" rel='stylesheet' type='text/css' media="all">
      <!--//stylesheets-->
      <link href="//fonts.googleapis.com/css?family=Cabin:400,500,600,700" rel="stylesheet">
      <link href="//fonts.googleapis.com/css?family=Roboto:300,400,500" rel="stylesheet">
   </head>
   <body>
      <div class="inner_page-banner" id="home">
         <!-- header -->
         <div class="headder-top d-lg-flex justify-content-between align-items-center py-3 px-sm-3">
            <!-- logo -->
            <div id="logo">
               <h1>
                  <a href="{{ route('index') }}">iHost</a>
               </h1>
            </div>
            <!-- //logo -->
            <!-- nav -->
            <nav>
               <label for="drop" class="toggle">Menu</label>
               <input type="checkbox" id="drop" />
               <ul class="menu">
                  <li>
                     <a href="{{ route('index') }}">Web</a>
                  </li>
                  <li>
                     <a href="{{ route('domain') }}">Domain</a>
                  </li>
                  <li>
                     <a href="{{ route('hosting') }}"  class="active">Hosting</a>
                  </li>
                  <li>
                     <a href="{{ route('email') }}">Email</a>
                  </li>
                  <a href="{{ route('contact') }}">Contact</a>
                  </li>
                  <li>
                     <a href="{{ route('project') }}">Our Projects</a>
                  </li>
               </ul>
            </nav>
            <!-- //nav -->
            <div class="d-flex mt-lg-1 mt-sm-2 mt-3 justify-content-center">
               <div class="search-w3layouts">
                  <!-- <span class="fa fa-users" aria-hidden="true"> -->
                  <button style="font-size:17px;padding: 8px 1px;">👩🏻‍💻 LOGIN</button>
                  </span>
                  <!-- modal form -->
                  <div class="popup">
                     <div class="content">
                        <span class="closebtn">&#9932;</span>
                        <div class="title">
                           <h1>Admin Login</h1>
                        </div>
                        <!-- <img src="https://webdevtrick.com/wp-content/uploads/logo-fb-1.png" alt="Car"> -->
                        <div class="subscribe">
                           <!-- <h1>Subscribe To Get The Notification Of Latest <span>POSTS</span></h1> -->
                           <form>
                              <input type="email" placeholder="Email Address">
                              <input type="password" placeholder="Password">
                              <input type="submit" value="Login">
                           </form>
                        </div>
                     </div>
                  </div>
                  <script src='js/jquery.min.js'></script>
                  <script src="js/modal.js"></script>
                  <!-- Modal login form -->
               </div>
            </div>
         </div>
         <div class="page-name teax-center">
            <h5>မြန်မာပြည်တွင် စိတ်ချရသော  <span style="color: #60e1bd;">Web Hosting Packages</span></h5>
         </div>
      </div>
      <!-- //header -->
      <div class="using-border py-3">
         <div class="inner_breadcrumb  ml-4">
            <ul class="short_ls text-center">
               <li>
                  <a href="index.html">Home</a>
                  <span>/ /</span>
               </li>
               <li>Hosting</li>
            </ul>
         </div>
      </div>
      <!-- pricing table -->
      <div class="webbody">
         <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
               <p class="webpackages"> iHost မှ စိတ်အချရဆုံးနှင့် အမြန်ဆုံး  <span style="color: #60e1bd;">Web Packages</span>  များ </p>
               <div class="comparison">
                  <table>
                     <thead>
                        <tr>
                           <th></th>
                           <th class="qbse">
                              Plan S
                           </th>
                           <th class="qbse">
                              Plan M
                           </th>
                           <th class="qbse">
                              Plan L
                           </th>
                        </tr>
                        <tr>
                           <th></th>
                        </tr>
                     </thead>
                     <tbody>
                        <tr>
                           <td></td>
                           <td colspan="4">Price</td>
                        </tr>
                        <tr class="compare-row">
                           <td style="font-size:20px"><b>Price<b></td>
                           <td>60,000 MMK/Year</td>
                           <td>132,000 MMK/Year</td>
                           <td>240,000 MMK/Year</td>
                        </tr>
                        <tr>
                           <td>&nbsp;</td>
                           <td colspan="4">Domain Name</td>
                        </tr>
                        <tr>
                           <td class="coloredrow" style="font-size:20px"><b>Domain Name<b></td>
                           <td class="coloredrow">1 Domain Name</td>
                           <td class="coloredrow">2 Domain Name</td>
                           <td class="coloredrow">Unlimited Domain Name</td>
                        </tr>
                        <tr>
                           <td>&nbsp;</td>
                           <td colspan="4">SSD</td>
                        </tr>
                        <tr class="compare-row">
                           <td style="font-size:20px"><b>SSD<b></td>
                           <td>10GB SSD Web + Email Storage</td>
                           <td>100GB SSD Web + Email Storage</td>
                           <td>400GB SSD Web + Email Storage</td>
                        </tr>
                        <tr>
                           <td>&nbsp;</td>
                           <td colspan="4">Alias Domain</td>
                        </tr>
                        <tr class="coloredrow">
                           <td style="font-size:20px"> <b>Alias Domain<b> </td>
                           <td>1 Alias Domain</td>
                           <td>5 Alias Domain</td>
                           <td>Unlimited Alias Domain</td>
                        </tr>
                        <tr>
                           <td>&nbsp;</td>
                           <td colspan="4"> Sub Domain </td>
                        </tr>
                        <tr class="compare-row">
                           <td  style="font-size:20px"> <b>Sub Domain<b></td>
                           <td>10 Sub Domain</td>
                           <td>25 Sub Domain</td>
                           <td>Unlimited Sub Domain</td>
                        </tr>
                        <tr>
                           <td>&nbsp;</td>
                           <td colspan="4"> Email Account </td>
                        </tr>
                        <tr class="coloredrow">
                           <td style="font-size:20px"> <b> Email Account <b> </td>
                           <td>20 Email Accounts</td>
                           <td>200 Email Accounts</td>
                           <td>1000 Email Accounts</td>
                        </tr>
                        <tr>
                           <td>&nbsp;</td>
                           <td colspan="4"> </td>
                        </tr>
                        <tr class="compare-row">
                           <td style="font-size:20px"> <b> FTP Account  <b></td>
                           <td>20 FTP Accounts</td>
                           <td>50 FTP Accounts</td>
                           <td>Unlimited FTP Account</td>
                        </tr>
                        <tr>
                           <td>&nbsp;</td>
                           <td colspan="4"> </td>
                        </tr>
                        <tr class="coloredrow">
                           <td style="font-size:20px"> <b> Database (My SQL)  <b> </td>
                           <td>5 Database (My SQL)</td>
                           <td>15 Database (My SQL)</td>
                           <td>Unlimited Database
                              <br/> (My SQL)
                           </td>
                        </tr>
                        <tr>
                           <td>&nbsp;</td>
                           <td colspan="4"> </td>
                        </tr>
                        <tr class="compare-row">
                           <td style="font-size:20px"> <b> Database Storage  <b></td>
                           <td> 1GB Database Storage</td>
                           <td>1GB Database Storage</td>
                           <td>1GB Database Storage</td>
                        </tr>
                        <tr>
                           <td>&nbsp;</td>
                           <td colspan="4"> </td>
                        </tr>
                        <tr class="coloredrow">
                           <td style="font-size:20px"> <b> Bandwidth  <b></td>
                           <td>Unlimited Bandwidth</td>
                           <td>Unlimited Bandwidth</td>
                           <td>Unlimited Bandwidth</td>
                        </tr>
                        <tr>
                           <td>&nbsp;</td>
                           <td colspan="4">FREE!</td>
                        </tr>
                        <tr class="compare-row">
                           <td class="invisible"></td>
                           <td>FREE! SSL (https://)</td>
                           <td>FREE! SSL (https://)</td>
                           <td>FREE! SSL (https://)</td>
                        </tr>
                     </tbody>
                  </table>
                  <!-- pdf download -->
                  <div class="outs_more-buttn mt-lg-5 mt-md-4 mt-3">
                     <a href="hosting_packages.pdf"><!-- <span class="fa fa-angle-down" aria-hidden="true"></span> -->👆  ယခု Price List အား Download ရယူရန်</a>
                  </div>
                  <!-- pdf download -->
               </div>
            </div>
         </div>
      </div>
      <!-- pricing table -->  
      <!-- hostinginfo+ssl -->
      <div class="wholebody">
         <div class="webbody1">
            <div class="container col-lg-12 col-md-12 col-sm-12" style="background-color:gray;">
               <h3 class="adventages">iHost မှ ရောင်းချသည့် Hosting ၏ အားသာချက်များ</h3>
               <div class="row">
                  <div class="col-lg-7 col-md-7 col-sm-7">
                     <div class="listofwebbody1">
                        <p>Apache နှင့် php module များ၊ setting များကို မည်သည့် website နှင့်မဆို
                           <br> အဆင်ပြေစေရန် တည်ဆောင်ပေးထားခြင်း။
                        </p>
                        <p>Hosting Control Panel(cPanel)မှာ အသုံးပြုရ လွယ်ကူခြင်း။</p>
                        <p>Backup နှင့် 99% Server Uptime အာမခံမှုရှိခြင်း။</p>
                        ၂၄ နာရီ ရရက်ပတ်လုံး Server Monitoring လုပ်ပေးခြင်း။
                        <p>အခမဲ့ Technical Support လုပ်ပေးခြင်း။</p>
                        <p></p>
                     </div>
                     <div class="adventages1">
                        <b>
                        iHost ရဲ့ web hosting ဟာ cloud system ကိုအသုံးပြုထားသောကြောင့်<br>
                        နိုင်ငံတကာအဆင့်မီ device တွေကို Server, Storage နဲ့ network 
                        တွေမှာအသုံးပြုထားလို့ <br> ဘယ်လိုအရေးပေါ် အခြေအနေမှာမဆို 
                        100% Down Time မရှိတဲ့ စနစ်ဖြစ်ခြင်းကြောင့်  <br> ယုံကြည်စိတ်ချစွာ အသုံးပြုနိုင်ပါသည်။ </b>
                     </div>
                  </div>
                  <div class="col-lg-5 col-md-5 col-sm-5">
                     <img src="images/web-hosting.png" class="web-hosting">
                  </div>
               </div>
            </div>
         </div>
         <div class="webbody2">
            <div class="container col-lg-12 col-md-12" style="background-color:#60e1bd">
               <h3 class="integration">Lifetime Free SSL integration</h3>
               <div class="row">
                  <div class="col-lg-5 col-md-5 col-sm-5">
                     <img class="ssl" src="images/ssl.png">
                  </div>
                  <div class="col-lg-7 col-md-7 col-sm-7">
                     <p class="businesstext">Business Planning</p>
                     <p class="businesstext1"> သင့် website ကို SSL အသုံးပြုခြင်းဖြင့် ပိုမိုလုံခြုံစိတ်ချ စေရန်အတွက်၎င်း၊ Google page မှာ rank ပိုမြင့်လာစေရန် အတွက်၎င်း ihost မှ DV SSL ကို တစ်သက်တာ provide လုပ်ပေးနေပါပြီ။</p>
                     <p class="businesstext2"></p>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--//hostinginfo+ssl -->
      @include('footer')

