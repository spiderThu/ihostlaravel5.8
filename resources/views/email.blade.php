<!DOCTYPE html>
<html lang="en">
    <head>
        <title>iHost : Web Hosting & Domain</title>
        <!--meta tags -->
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="keywords" content="ihost,iHost,domain,hosting,web hosting,email,myanmar hosting,spider"/>
        <LINK rel="SHORTCUT ICON" href="images/ishortcut.png">
        <script>
            addEventListener("load", function () {
                setTimeout(hideURLbar, 0);
            }, false);

            function hideURLbar() {
                window.scrollTo(0, 1);
            }
        </script>
        <!--booststrap-->
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
        <!--//booststrap end-->
        <!-- font-awesome icons -->
        <link href="css/font-awesome.min.css" rel="stylesheet">
        <!-- //font-awesome icons -->
        <!--stylesheets-->
        <link href="css/style.css" rel='stylesheet' type='text/css' media="all">
        <link href="css/responsive.css" rel='stylesheet' type='text/css' media="all">
        <!--//stylesheets-->
        <link href="//fonts.googleapis.com/css?family=Cabin:400,500,600,700" rel="stylesheet">
        <link href="//fonts.googleapis.com/css?family=Roboto:300,400,500" rel="stylesheet">
    </head>
    <body>
        <div class="inner_page-banner" id="home">
            <!-- header -->
            <div class="headder-top d-lg-flex justify-content-between align-items-center py-3 px-sm-3">
                <!-- logo -->
                <div id="logo">
                    <h1>
                        <a href="{{ route('index') }}">iHost</a>
                    </h1>
                </div>
                <!-- //logo -->
                <!-- nav -->
                <nav>
                    <label for="drop" class="toggle">Menu</label>
                    <input type="checkbox" id="drop" />
                    <ul class="menu">
                        <li>
                            <a href="{{ route('index') }}">Web</a>
                        </li>
                        <li>
                            <a href="{{ route('domain') }}">Domain</a>
                        </li>
                        <li>
                            <a href="{{ route('hosting') }}">Hosting</a>
                        </li>
                        <li>
                            <a href="{{ route('email') }}" class="active">Email</a>
                        </li>
                        <li>
                            <a href="{{ route('contact') }}">Contact</a>
                        </li>
                        <li>
                            <a href="{{ route('project') }}">Our Projects</a>
                        </li>
                    </ul>
                </nav>
                <!-- //nav -->
                <div class="d-flex mt-lg-1 mt-sm-2 mt-3 justify-content-center">
                    
                    <div class="search-w3layouts">
                        <!-- <span class="fa fa-users" aria-hidden="true"> -->
                            <button style="font-size:17px;padding: 8px 1px;">👩🏻‍💻 LOGIN</button>
                        </span>

                            <!-- modal form -->
                            <div class="popup">
                                <div class="content">

                                    <span class="closebtn">&#9932;</span>

                                    <div class="title">
                                        <h1>Admin Login</h1>
                                    </div>

                                    <!-- <img src="https://webdevtrick.com/wp-content/uploads/logo-fb-1.png" alt="Car"> -->

                                    <div class="subscribe">
                                        <!-- <h1>Subscribe To Get The Notification Of Latest <span>POSTS</span></h1> -->

                                        <form>
                                            <input type="email" placeholder="Email Address">
                                            <input type="password" placeholder="Password">
                                            <input type="submit" value="Login">
                                        </form>
                                    </div>

                                </div>
                            </div>
                            <script src='js/jquery.min.js'></script>
                            <script src="js/modal.js"></script>
                            <!-- Modal login form -->

                    </div>
                </div>
            </div>
            <div class="page-name teax-center">
                <h5>မိမိလုပ်ငန်းအမည်ဖြင့်  <span style="color: #60e1bd;">Email</span> ပိုင်ဆိုင်နိုင်မည့် <span style="color: #60e1bd;"></span> အခွင့်အရေး </h5>
            </div>
        </div>
        <!-- //header -->
        <div class="using-border py-3">
            <div class="inner_breadcrumb  ml-4">
                <ul class="short_ls text-center">
                    <li>
                        <a href="index.html">Home</a>
                        <span>/ /</span>
                    </li>
                    <li>Email</li>
                </ul>
            </div>
        </div>
        <!--//banner -->

        <!-- what is email -->
        <section class="email py-lg-4 py-md-3 py-sm-3 py-3">
            <div class="container py-lg-5 py-md-5 py-sm-4 py-3">
                <h5 class="own-email">ကိုယ်ပိုင်  <span style="color: #60e1bd;">Email Name</span> ဆိုတာ ဘာလဲ?</h5>
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4 text-center my-4 counter-list-grid">
                        <p class="email-info">
                            မိမိလုပ်ငန်းအမည် နဲ့ Email Service တစ်ခုကိုအသုံးပြုနိုင်ခြင်းဖြစ်ပါသည်။
                            Webmail နှင့်သာမက Outlook(POP3,SMTP,IMAP)
                            တွေမှာပါအသုံးပြုနိုင်ပါတယ်။Emailတွေထဲ Spam တွေ မဝင်အောင်
                            စစ်ထုတ်ပေးခြင်းနဲ့ Virus ကာကွယ်ပေးခြင်းများအား ပြုလုပ်ပေးပါတယ်။
                            ပြည်တွင်းရောပြည်ပမှာသုံးနေတဲ့ Server Bandwith အတိုင်းဖြစ်တဲ့အတွက်
                            လုပ်ဆောင်မှုမြန်ဆန်ပြီး နှောင့်နှေးကြန့်ကြာခြင်းမရှိကြောင်းအာမခံပါသည်။

                        </p>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-8 text-center my-8 counter-list-grid">
                        <img src="images/email-server.jpg" class="e-server">
                    </div>
                </div>
            </div>
        </section>
        <!--//what is email -->

        <!-- counter -->
        <section class="counter py-lg-4 py-md-3 py-sm-3 py-3">
            <div class="container py-lg-5 py-md-5 py-sm-4 py-3">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 text-center my-12 counter-list-grid">
                        <p class="email-green">သတ်မှတ်ထားသော Storage အတွင့် Accountအသစ်များစိတ်ကြိုက်ဖွင့်နိုင်ခြင်း။
                            Mail Severအတွက်အထူးသီးသန့် SSD ကိုအသုံးပြုထားခြင်း။
                            လုပ်ငန်းအမည် မိမိစိတ်ကြိုက် Domain Name ဖြင့် Email အသုံးပြုနိုင်ခြင်း။
                            Virus နဲ့ Spam အားအချိန်နှင့်တပြေးညီကာကွယ်ပေးထားခြင်း။
                            Cloud Server ကိုအသုံးပြုထားခြင်းကြောင့် Downtime မရှိခြင်း။
                            ကိုယ်တိုင်စီမံထိန်းချုုပ်နိုင်သော Control Panel ပေးထားခြင်း။
</p>
                    </div>
                </div>
            </div>
        </section>
        <!--//counter -->

        <!-- about -->
        <section class="about py-lg-4 py-md-3 py-sm-3 py-3">
            <div class="container py-lg-5 py-md-5 py-sm-4 py-3">
                <div class="row" style="margin: -2em 0em 2em 0em">
                    <div class="col-lg-3 col-md-6 col-sm-6 my-3 about-grid-gried">
                        <div class="about-fashion-grid ">
                            <div class="about-icon text-center">
                                <!-- <span class="fa fa-cloud" aria-hidden="true"></span> -->
                                <img src="images/cloudserver.png" style="padding: 5px;">
                            </div>
                            <div class="about-wthree-grid mb-3  mt-3">
                                <h4>Cloud Server</h4>
                            </div>
                            <p>Cloud Server ပေါ်မှာ Email Service လုပ်ထားတဲ့အတွက် server device
                                ချို့ယွင်းရင်တောင် DownTimeမဖြစ်စေရန် ၁၀၀%အာမခံခြင်း။
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 my-3 about-grid-gried">
                        <div class="about-fashion-grid ">
                            <div class="about-icon text-center">
                                <!-- <span class="fa fa-users" aria-hidden="true"></span> -->
                                <img src="images/emailaccount.png" style="padding: 5px;">
                            </div>
                            <div class="about-wthree-grid mb-3  mt-3">
                                <h4>Email Accounts</h4>
                            </div>
                            <p>မိမိဝယ်ယူထားသော Storage အရ
                                Customer လိုအပ်သလောက်
                                အကောင့်အရေအတွက်
                                အကန့်အသတ်မရှိ Email Account
                                အသုံးပြုနိုင်ခြင်း။</p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 my-3 about-grid-gried">
                        <div class="about-fashion-grid " style="height: 328px;">
                            <div class="about-icon text-center">
                                <!-- <span class="fa fa-briefcase" aria-hidden="true"></span> -->
                                <img src="images/controlpanel.png" style="padding: 5px;">
                            </div>
                            <div class="about-wthree-grid mb-3 mt-3">
                                <h4>Control Panel</h4>
                            </div>
                            <p>အကောင်းဆုံးနှင့် သုံးရအလွယ်ကူဆုံး
                                Control Panel မှတဆင့် User ကိုယ်တိုင်
                                account များကိုထိန်းချုပ်စီမံနိုင်ခြင်း။
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 my-3 about-grid-gried">
                        <div class="about-fashion-grid ">
                            <div class="about-icon text-center">
                                <!-- <span class="fa fa-cubes" aria-hidden="true"></span> -->
                                <img src="images/outlookmail.png" style="padding: 5px;">
                            </div>
                            <div class="about-wthree-grid mb-3 mt-3">
                                <h4>Webmail,Outlook</h4>
                            </div>
                            <p>Email Server System အား Webmail,Outlook, IMAP,SMTP and SSL Certificate တွေနှင့် ချိတ်ဆက်အသုံးပြုနိုင်အောင် စီစဉ်ပေးထားခြင်း။</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--//about -->

        @include('footer')