<!DOCTYPE html>
<html lang="en">
   <head>
      <title>iHost : Web Hosting & Domain</title>
      <!--meta tags -->
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content="ihost,iHost,domain,hosting,web hosting,email,myanmar hosting,spider" />
      <LINK rel="SHORTCUT ICON" href="images/ishortcut.png">
      <link href="css/main.css" rel="stylesheet" />
      <script src="js/bootstrap.min.js"></script>
      <script>
         addEventListener("load", function() {
             setTimeout(hideURLbar, 0);
         }, false);
         
         function hideURLbar() {
             window.scrollTo(0, 1);
         }
      </script>
      <!--booststrap-->
      <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
      <!--//booststrap end-->
      <!-- font-awesome icons -->
      <link href="css/font-awesome.min.css" rel="stylesheet">
      <!-- //font-awesome icons -->
      <!--stylesheets-->
      <link href="css/style.css" rel='stylesheet' type='text/css' media="all">
      <link href="css/responsive.css" rel='stylesheet' type='text/css' media="all">
      <!--//stylesheets-->
      <link href="//fonts.googleapis.com/css?family=Cabin:400,500,600,700" rel="stylesheet">
      <link href="//fonts.googleapis.com/css?family=Roboto:300,400,500" rel="stylesheet">
      <script src = "https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
      <script>
         $(document).ready(function(myFunction) {
           $("#btns").click(function() {
              //window.location = "#tabMenu";
              $('html, body').animate({ scrollTop: $('#tabMenu').offset().top }, 'slow');
           });
         });
      </script>
      <!-- <script>
         function myFunction() {
           alert("Page is loaded");
         }
         </script> -->
      <!--  <script>
         document.getElementById("#tabMenu").onload = function() {myFunction()};
         
         function myFunction() {
           window.location = "#tabMenu";
         }
         </script> -->
   </head>
   <body>
      <div class="inner_page-banner" id="home">
         <!-- header -->
         <div class="headder-top d-lg-flex justify-content-between align-items-center py-3 px-sm-3">
            <!-- logo -->
            <div id="logo">
               <h1>
                  <a href="{{ route('index') }}">iHost</a>
               </h1>
            </div>
            <!-- //logo -->
            <!-- nav -->
            <nav>
               <label for="drop" class="toggle">Menu</label>
               <input type="checkbox" id="drop" />
               <ul class="menu">
                  <li>
                     <a href="{{ route('index') }}">Web</a>
                  </li>
                  <li>
                     <a href="{{ route('domain') }}" class="active">Domain</a>
                  </li>
                  <li>
                     <a href="{{ route('hosting') }}">Hosting</a>
                  </li>
                  <li>
                     <a href="{{ route('email') }}">Email</a>
                  </li>
                  <a href="{{ route('contact') }}">Contact</a>
                  </li>
                  <li>
                     <a href="{{ route('project') }}">Our Projects</a>
                  </li>
               </ul>
            </nav>
            <!-- //nav -->
            <div class="d-flex mt-lg-1 mt-sm-2 mt-3 justify-content-center">
               <div class="search-w3layouts">
                  <!-- <span class="fa fa-users" aria-hidden="true"> -->
                  <button style="font-size:17px;padding: 8px 1px;">👩🏻‍💻 LOGIN</button>
                  </span>
                  <!-- modal form -->
                  <div class="popup">
                     <div class="content">
                        <span class="closebtn">&#9932;</span>
                        <div class="title">
                           <h1>Admin Login</h1>
                        </div>
                        <!-- <img src="https://webdevtrick.com/wp-content/uploads/logo-fb-1.png" alt="Car"> -->
                        <div class="subscribe">
                           <!-- <h1>Subscribe To Get The Notification Of Latest <span>POSTS</span></h1> -->
                           <form>
                              <input type="email" placeholder="Email Address">
                              <input type="password" placeholder="Password">
                              <input type="submit" value="Login">
                           </form>
                        </div>
                     </div>
                  </div>
                  <script src='js/jquery.min.js'></script>
                  <script src="js/modal.js"></script>
                  <!-- Modal login form -->
               </div>
            </div>
         </div>
         <div class="page-name teax-center">
            <h5>မြန်မာပြည်တွင်စျေးသက်သာသော <span style="color: #60e1bd;">Domain</span> ဝန်ဆောင်မှု</h5>
         </div>
      </div>
      <!-- //header -->
      <div class="using-border py-3">
         <div class="inner_breadcrumb  ml-4">
            <ul class="short_ls text-center">
               <li>
                  <a href="index.html">Home</a>
                  <span>/ /</span>
               </li>
               <li>Domain</li>
            </ul>
         </div>
      </div>
      <!-- domain body -->
      <!-- new domain price table -->
      <div class="caption">Domain စျေးနှုန်းများ</div>
      <div id="table">
         <div class="header-row roww">
            <span class="cell primary" style="color:#fff;">Domain Name</span>
            <span class="cell" style="color:#fff;">Domain Price</span>
         </div>
         <div class="roww">
            <input type="radio" name="expand">
            <span class="cell primary" data-label="Domain Names">.com</span>
            <span class="cell" data-label="Prices">12,000 MMK/yr</span>
         </div>
         <div class="roww">
            <input type="radio" name="expand">
            <span class="cell primary" data-label="Domain Names">.com.mm</span>
            <span class="cell" data-label="Prices">70,000  MMK/yr</span>  
         </div>
         <div class="roww">
            <input type="radio" name="expand">
            <span class="cell primary" data-label="Domain Names">.org</span>
            <span class="cell" data-label="Prices">17,500  MMK/yr</span>
         </div>
         <div class="roww">
            <input type="radio" name="expand">
            <span class="cell primary" data-label="Domain Names">.org.mm</span>
            <span class="cell" data-label="Prices">70,000  MMK/yr</span>
         </div>
         <div class="roww">
            <input type="radio" name="expand">
            <span class="cell primary" data-label="Domain Names">.net</span>
            <span class="cell" data-label="Prices">17,500  MMK/yr</span>
         </div>
         <div class="roww">
            <input type="radio" name="expand">
            <span class="cell primary" data-label="Domain Names">.net.mm</span>
            <span class="cell" data-label="Prices">70,000  MMK/yr</span>  
         </div>
         <div class="roww">
            <input type="radio" name="expand">
            <span class="cell primary" data-label="Domain Names">.info</span>
            <span class="cell" data-label="Prices">5,000  MMK/yr</span>  
         </div>
         <div class="roww">
            <input type="radio" name="expand">
            <span class="cell primary" data-label="Domain Names">.shop</span>
            <span class="cell" data-label="Prices">17,500  MMK/yr</span>  
         </div>
         <div class="roww">
            <input type="radio" name="expand">
            <span class="cell primary" data-label="Domain Names">.asia</span>
            <span class="cell" data-label="Prices">7,000  MMK/yr</span>  
         </div>
         <div class="roww">
            <input type="radio" name="expand">
            <span class="cell primary" data-label="Domain Names">.club</span>
            <span class="cell" data-label="Prices">3,000  MMK/yr</span>  
         </div>
         <div class="roww">
            <input type="radio" name="expand">
            <span class="cell primary" data-label="Domain Names">.biz</span>
            <span class="cell" data-label="Prices">20,000  MMK/yr</span>  
         </div>
         <div class="roww">
            <input type="radio" name="expand">
            <span class="cell primary" data-label="Domain Names">.pw</span>
            <span class="cell" data-label="Prices">3,000  MMK/yr</span>  
         </div>
         <div class="roww">
            <input type="radio" name="expand">
            <span class="cell primary" data-label="Domain Names">.co</span>
            <span class="cell" data-label="Prices">20,000  MMK/yr</span>  
         </div>
         <div class="roww">
            <input type="radio" name="expand">
            <span class="cell primary" data-label="Domain Names">.me</span>
            <span class="cell" data-label="Prices">7,000  MMK/yr</span>  
         </div>
         <div class="roww">
            <input type="radio" name="expand">
            <span class="cell primary" data-label="Domain Names">.work</span>
            <span class="cell" data-label="Prices">3,000  MMK/yr</span>  
         </div>
         <div class="roww">
            <input type="radio" name="expand">
            <span class="cell primary" data-label="Domain Names">.xyz</span>
            <span class="cell" data-label="Prices">3,000  MMK/yr</span>  
         </div>
         <div class="roww">
            <input type="radio" name="expand">
            <span class="cell primary" data-label="Domain Names">.fun</span>
            <span class="cell" data-label="Prices">3,000  MMK/yr</span>  
         </div>
         <div class="roww">
            <input type="radio" name="expand">
            <span class="cell primary" data-label="Domain Names">.biz.mm</span>
            <span class="cell" data-label="Prices">70,000  MMK/yr</span>  
         </div>
         <div class="roww">
            <input type="radio" name="expand">
            <span class="cell primary" data-label="Domain Names">.per.mm</span>
            <span class="cell" data-label="Prices">70,000  MMK/yr</span>  
         </div>
         <div class="roww">
            <input type="radio" name="expand">
            <span class="cell primary" data-label="Domain Names">.mobi</span>
            <span class="cell" data-label="Prices">7,000  MMK/yr</span>  
         </div>
         <div class="roww">
            <input type="radio" name="expand">
            <span class="cell primary" data-label="Domain Names">.tech</span>
            <span class="cell" data-label="Prices">7,000  MMK/yr</span>  
         </div>
         <div class="roww">
            <input type="radio" name="expand">
            <span class="cell primary" data-label="Domain Names">.site</span>
            <span class="cell" data-label="Prices">3,000  MMK/yr</span>  
         </div>
         <div class="roww">
            <input type="radio" name="expand">
            <span class="cell primary" data-label="Domain Names">.top</span>
            <span class="cell" data-label="Prices">3,000  MMK/yr</span>  
         </div>
         <div class="roww">
            <input type="radio" name="expand">
            <span class="cell primary" data-label="Domain Names">.website</span>
            <span class="cell" data-label="Prices">3,000  MMK/yr</span>  
         </div>
         <div class="roww">
            <input type="radio" name="expand">
            <span class="cell primary" data-label="Domain Names">.online</span>
            <span class="cell" data-label="Prices">3,000  MMK/yr</span>  
         </div>
         <div class="roww">
            <input type="radio" name="expand">
            <span class="cell primary" data-label="Domain Names">.services</span>
            <span class="cell" data-label="Prices">5,000  MMK/yr</span>  
         </div>
         <div class="roww">
            <input type="radio" name="expand">
            <span class="cell primary" data-label="Domain Names">.company</span>
            <span class="cell" data-label="Prices">10,000  MMK/yr</span>  
         </div>
         <div class="roww">
            <input type="radio" name="expand">
            <span class="cell primary" data-label="Domain Names">.ltd</span>
            <span class="cell" data-label="Prices">10,000  MMK/yr</span>  
         </div>
         <div class="roww">
            <input type="radio" name="expand">
            <span class="cell primary" data-label="Domain Names">.pro</span>
            <span class="cell" data-label="Prices">7,000  MMK/yr</span>  
         </div>
         <div class="roww">
            <input type="radio" name="expand">
            <span class="cell primary" data-label="Domain Names">.news</span>
            <span class="cell" data-label="Prices">15,000  MMK/yr</span>  
         </div>
         <div class="roww">
            <input type="radio" name="expand">
            <span class="cell primary" data-label="Domain Names">.inc</span>
            <span class="cell" data-label="Prices">1,000,000  MMK/yr</span>  
         </div>
         <div class="roww">
            <input type="radio" name="expand">
            <span class="cell primary" data-label="Domain Names">.la</span>
            <span class="cell" data-label="Prices">45,000  MMK/yr</span>  
         </div>
         <div class="roww">
            <input type="radio" name="expand">
            <span class="cell primary" data-label="Domain Names">.email</span>
            <span class="cell" data-label="Prices">5,000  MMK/yr</span>  
         </div>
         <div class="roww">
            <input type="radio" name="expand">
            <span class="cell primary" data-label="Domain Names">.cloud</span>
            <span class="cell" data-label="Prices">7,000  MMK/yr</span>  
         </div>
      </div>
      <div class="domaintablefooter">
         <div class="container col-lg-12 col-md-12 col-sm-12">
            .mm Domain မှတ်ပုံတင်ခြင်းအတွက်လိုအပ်သော ဝန်ဆောင်မှုများမှာ
            DNS Records ( Because there is no control panel provided for this domain )
            Company Registration (Copy)
            Form (6) and Form (26) (Copy)
            NRC Card or Passport of one Board member (Copy)
            Cover Letter with Company Letter Head -Contact information (Mail and Ph.no)
         </div>
      </div>
      <!-- // new domain price table -->
      <!-- nna new add search bar -->
      <div class="s130">
         <form action="{{ route('searchDomain') }}" method="GET">
            @csrf
            <p style="color:white;text-align: center; padding-bottom:80px;font-size:28px;"> မိတ်ဆွေတို့ စိတ်ကြိုက် Domain Name များကို ရွေးချယ်လိုက်ပါ.. </p>
            <div class="inner-form" id="tabMenu" onload>
               <div class="input-field first-wrap">
                  <div class="svg-wrapper">
                     <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z"></path>
                     </svg>
                  </div>
                  <input id="search" type="text" name="domain" placeholder="Search for a domain..." />
               </div>
               <div class="input-field second-wrap">
                  <button class="btn-search" type="submit" id="btns">SEARCH</button>
               </div>
            </div>
            <!-- @if (Session::has('already'))
               <script>
                 $( document ).ready(function() {
                   window.location = "#btns"; });
               </script>
               
                   <h3 class="info">{{ old('domain') }}</h3>
                   <span class="info" style="color:pink;">Already Register! 😟</span>
               @endif
               
               
               @if (Session::has('success'))
               <script>
                 $( document ).ready(function() {
               window.location = "#tabMenu"; });
               </script>
                   <h3 class="info">{{ old('domain') }}</h3>
                   <span class="info" style="color:cyan;">Available 😊 Not Taken, You can register it!</span>
               @endif -->
            <!--// domain search added -->
            <!-- <div class="outs_more-buttn mt-lg-5 mt-md-4 mt-3">
               <a href="" style="font-size: 16px;" class="domainsearch"> Buy Domain </a>
               </div> -->
         </form>
      </div>
      <!-- //nna new add search bar -->
      <div class="domainbody1">
         <div class="container col-lg-12 col-md-12 col-sm-12">
            <h5 class="owndomain">စိတ်ကြိုက်နာမည်နဲ့ 
               <span style="color: #60e1bd;"> Domain </span>
               များကို ရွေးချယ်ပြီး အလွယ်တကူဝယ်ယူ အသုံးပြုနိုင်ပါသည်... 
            </h5>
            <div class="row" style="background-color:white;">
               <div class="col-lg-5 col-md-12 col-sm-5">
                  <img class="domain-names" src="images/domain-names.png">
               </div>
               <div class="col-lg-7 col-md-7 col-sm-7">
                  <img class="domain-lists" src="images/domainlists.png">
               </div>
            </div>
         </div>
      </div>
      <div class="domainbody2">
         <div class="container col-lg-12 col-md-12 col-sm-12">
            <h5 class="contractinformation">  <span style="color:white;"> <img class= "protection-icon" src="images/protection-icon.png"> Domain  ရဲ့  Contract information </span> ကို ကာကွယ်ပေးထားသည့် စနစ်အား အခမဲ့ အသုံးပြုနိုင်ပါသည်။</h5>
            <div class="row" style="background-color:#60e1bd;">
               <div class="col-lg-7 col-md-7 col-sm-7">
                  <div class="contractinformation1">Domain ပိုင်ဆိုင်သူ အချက်အလက်များ မဖော်ပြလိုသော Customer များ အတွက်
                     <br> လုံခြုံမှုနှင့် စိတ်ချမှုရှိစေရန် Whois Privacy Service ကို
                     <br>Active ဖြစ်စေရန် Control Panel တွင် အခမဲ့ပြုလုပ်နိုင်ပါသည်။
                  </div>
               </div>
               <div class="col-lg-5 col-md-5 col-sm-5">
                  <img class="search-web" src="images/search-seo.png">
               </div>
            </div>
         </div>
      </div>
      <div class="domainbody3">
         <div class="container col-lg-12 col-md-12 col-sm-12">
            <h3 class="buydomain">iHost မှ့ Domain များ ဘာလို့ဝယ်သင့်လဲ?</h3>
            <div class="row" style="background-color:#f9f9f9;">
               <div class="col-lg-6 col-md-6 col-sm-6">
                  <img class="cpanel-domain" src="images/cpanel.png">
                  <div class="buydomain1">
                     Domain Control Panel ကနေ အလွယ်တကူ
                     <br>အသုံးပြုရလွယ်ကူခြင်း။Nameserver လွယ်ကူစွာပြောင်း၍ မည်သည့် Hosting နှင့်မဆို အသုံးပြုနိုင်ခြင်း။ တစ်နှစ်စာနှုန်းထားဖြင့် စျေးနှုန်းသက်သာစွာ ဝယ်ယူနိုင်ခြင်း။
                     <br>အခမဲ့ Technical Support ပေးခြင်း။
                  </div>
               </div>
               <div class="col-lg-6 col-md-6 col-sm-6">
                  <div class="cashdomain">
                     <img class="dollar-domain" src="images/dollar-domain.png">
                     <br>
                     <img src="images/credit-card.png"> 
                     <p class="d-pt">
                        Credit/Debit Card(သို့)PayPal အသုံးပြုပြီး တိုက်ရိုက်ဝယ်ယူနိုင်ပါသည်။
                     </p>
                     <img src="images/office-cash.png">
                     <p class="d-pt">
                        ငွေသားဖြင့်ပေးချေမည်ဆိုပါက ရုံးသို့လာရောက်ဝယ်ယူနိုင်ပါသည်။
                     </p>
                     <img src="images/bank-cash.png">
                     <p class="d-pt">
                        နယ်မှ့ ဝယ်ယူသူများလည်း Bank Account သို့ ငွေလွဲဝယ်ယူနိုင်ပါသည်။
                     </p>
                     <img src="images/phone-icon.png">
                     <p class="d-pt">ရုံးဖုန်းနံပါတ် - 09977870421 , 09977870422
                     </p>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- domain body -->
      <!-- <script>
         //redirect to specific tab
         $(document).ready(function () {
           $( "#btns" ).click(function() {
             alert( "Handler for .click() called." );
           });
           // $('#tabMenu a[href="#{{ old('tab') }}"]').tab('show')
           // });
         </script> -->
      @include('footer')