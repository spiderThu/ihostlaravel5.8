<!-- footer -->
      <section class="footer py-lg-4 py-md-3 py-sm-3 py-3" style="background: #fff;">
         <div class="container py-lg-5 py-md-5 py-sm-4 py-3">
            <div class="row mt-lg-4 mt-3">
               <div class="col-lg-5 col-md-4 footer-titles my-3">
                  <h2>
                     <a href="index.html">iHost</a>
                  </h2>
                  <p class="pt-3">
                     <img src="images/location-icon.png">
                    အခန်း(၅) ၊ သစ်တောလမ်း ၊
                     <br/>  တရုတ်ဘုံကျောင်းအနီး ၊ ပြင်ဦးလွင်မြို့။
                  </p>
                  <p class="pt-3">
                     <img src="images/location-icon.png"> Room (5),Thit Taw Street, Near Chinese Temple,
                     <br>Pyin Oo Lwin, Myanmar.
                  </p>
                  <p class="pt-3">
                     <img src="images/phone-icon.png"> +(95) 9977870421 , +(95) 9795938304
                  </p>
                  <p class="pt-3">
                     <img src="images/email-icon.png"><a href="mailto:info@spidernetworkict.com"> info@spidernetworkict.com</a>
                  </p>
                  <p class="pt-3">
                     Authorized by <br> <a href="https://www.facebook.com/spidernetworkgroup/" target="_blank" style="color: #60e1bd;text-transform: uppercase;"><img src="images/spiderlogo.png" style="width:50%;"></a>
                  </p>
               </div>
               <div class="col-lg-3 col-md-4 footer-titles my-3">
                  <h4 class="mb-lg-4 mb-3">Quick links</h4>
                  <ul class="bottom-menu ">
                     <li class="pb-sm-2 pb-1">
                        <a href="{{ route('index') }}">Web</a>
                     </li>
                     <li class="py-sm-2 py-1">
                        <a href="{{ route('domain') }}">Domain</a>
                     </li>
                     <li class="py-sm-2 py-1">
                        <a href="{{ route('hosting') }}">Hosting</a>
                     </li>
                     <li class="py-sm-2 py-1">
                        <a href="{{ route('email') }}">Email</a>
                     </li>
                     <li class="py-sm-2 py-1">
                        <a href="{{ route('contact') }}">Contact</a>
                     </li>
                     <li class="py-sm-2 py-1">
                        <a href="{{ route('project') }}">Our Projects</a>
                     </li>
                  </ul>
               </div>
               <div class="footer-titles col-lg-4 col-md-4 my-3">
                  <div class="social-icons mt-3">
                     <img src="images/iHost.png" class="footer-logo"/>
                     <h5 class="p-foot-text">
                        iHost helps your business more better.
                        <br>iHost is your perfect partner to help move forward on the internet, because as a local company,
                        we will be with you every step of the way,
                        with full phone support and comprehensive email support as well.
                     </h5>
                  </div>
               </div>
            </div>
            <!-- move icon -->
            <div class="text-center mt-lg-5 mt-md-4 mt-3">
               <a href="#home" class="move-top text-center mt-3">
               <!-- <span class="fa fa-long-arrow-up" aria-hidden="true"></span> -->
               👆🏻
               </a>
            </div>
            <!--//move icon -->
         </div>
      </section>
      <!--//footer -->
      <!-- copy right -->
      <footer>
         <div class="bottom-footer text-center py-md-4 py-3">
            <p>
               © 2019 iHost. All Rights Reserved | Design by
               <a href="https://www.facebook.com/thuthuaung29/?modal=admin_todo_tour" target="_blank">T2A</a>
                <!-- Start of CuterCounter Code -->
               <div style="float:right;padding-right:10px;">
               <img src="https://www.cutercounter.com/hits.php?id=hxddkad&nd=8&style=3" border="0" alt="website counter">
               </div>
               <!-- End of CuterCounter Code -->
            </p>
         </div>
      </footer>
      <!-- copy right -->
   </body>
</html>