<!DOCTYPE html>
<html lang="en">
   <head>
      <title>iHost : Web Hosting & Domain</title>
      <!--meta tags -->
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content="ihost,iHost,domain,hosting,web hosting,email,myanmar hosting,spider"/>
      <LINK rel="SHORTCUT ICON" href="images/ishortcut.png">
      <script>
         addEventListener("load", function () {
           setTimeout(hideURLbar, 0);
         }, false);
         
         function hideURLbar() {
           window.scrollTo(0, 1);
         }
      </script>
      <!--booststrap-->
      <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
      <!--//booststrap end-->
      <!-- font-awesome icons -->
      <link href="css/font-awesome.min.css" rel="stylesheet">
      <!-- //font-awesome icons -->
      <!--stylesheets-->
      <link href="css/style.css" rel='stylesheet' type='text/css' media="all">
      <link href="css/responsive.css" rel='stylesheet' type='text/css' media="all">
      <!--//stylesheets-->
      <link href="//fonts.googleapis.com/css?family=Cabin:400,500,600,700" rel="stylesheet">
      <link href="//fonts.googleapis.com/css?family=Roboto:300,400,500" rel="stylesheet">
   </head>
   <body> 
    @if (Session::has('success'))
      <script type="text/javascript"> 
         alert("Your message had been send successfully!");
      </script>
    @endif
      <div class="inner_page-banner" id="home">
         <!-- header -->
         <div class="headder-top d-lg-flex justify-content-between align-items-center py-3 px-sm-3">
            <!-- logo -->
            <div id="logo">
               <h1>
                  <a href="{{ route('index') }}">iHost</a>
               </h1>
            </div>
            <!-- //logo -->
            <!-- nav -->
            <nav>
               <label for="drop" class="toggle">Menu</label>
               <input type="checkbox" id="drop" />
               <ul class="menu">
                  <li>
                     <a href="{{ route('index') }}">Web</a>
                  </li>
                  <li>
                     <a href="{{ route('domain') }}">Domain</a>
                  </li>
                  <li>
                     <a href="{{ route('hosting') }}">Hosting</a>
                  </li>
                  <li>
                     <a href="{{ route('email') }}">Email</a>
                  </li>
                  <li>
                     <a href="{{ route('contact') }}">Contact</a>
                  </li>
                  <li>
                     <a href="{{ route('project') }}" class="active">Our Projects</a>
                  </li>
               </ul>
            </nav>
            <!-- //nav -->
            <div class="d-flex mt-lg-1 mt-sm-2 mt-3 justify-content-center">
                    
                    <div class="search-w3layouts">
                        <!-- <span class="fa fa-users" aria-hidden="true"> -->
                            <button style="font-size:17px;padding: 8px 1px;">👩🏻‍💻 LOGIN</button>
                        </span>

                            <!-- modal form -->
                            <div class="popup">
                                <div class="content">

                                    <span class="closebtn">&#9932;</span>

                                    <div class="title">
                                        <h1>Admin Login</h1>
                                    </div>

                                    <!-- <img src="https://webdevtrick.com/wp-content/uploads/logo-fb-1.png" alt="Car"> -->

                                    <div class="subscribe">
                                        <!-- <h1>Subscribe To Get The Notification Of Latest <span>POSTS</span></h1> -->

                                        <form>
                                            <input type="email" placeholder="Email Address">
                                            <input type="password" placeholder="Password">
                                            <input type="submit" value="Login">
                                        </form>
                                    </div>

                                </div>
                            </div>
                            <script src='js/jquery.min.js'></script>
                            <script src="js/modal.js"></script>
                            <!-- Modal login form -->

                    </div>
                </div>
         </div>
         <div class="page-name teax-center">
            <h5>Our Projects</h5>
         </div>
      </div>
      <!-- //header -->
      <div class="using-border py-3">
         <div class="inner_breadcrumb  ml-4">
            <ul class="short_ls text-center">
               <li>
                  <a href="index.html">Home</a>
                  <span>/ /</span>
               </li>
               <li>Contact</li>
            </ul>
         </div>
      </div>
      <!-- contact -->
      <section class="contact py-lg-4 py-md-3 py-sm-3 py-3" style="background-color: #f9f9f9;">
         <div class="container py-lg-5 py-md-5 py-sm-4 py-3">
            <div class="row">
               <div class="col-lg-6 col-md-6">
                 <img class="ipresponsive" src="images/ip_responsive.png"> <br> <br>
                </div>
               <!--//texts -->
               <div class="col-lg-6 col-md-6">
                <br><br><br>
                      <h5 class="own-email" style="text-align: center;"> Intelligence Power Co.,Ltd.</h5>
                
                   <br><br>
                        <p class="email-info" style="text-align: center;padding-left:30px;">
                           Intelligence Power Co.,Ltd. အတွက် Website ကို iHost မှ တာဝန်ယူဆောင်ရွက်ပေးထားပါသည်။<br>
                           <a href="https://intellpower.net/" target="blank_">https://intellpower.net/</a>

                        </p>
                    </div>
               <!-- texts -->
            </div>
         </div>
      </section>
       <section class="contact py-lg-4 py-md-3 py-sm-3 py-3" style="background-color: #f9f9f9;">
         <div class="container py-lg-5 py-md-5 py-sm-4 py-3">
            <div class="row">
               <!--//texts -->
                    <div class="col-lg-6 col-md-6">
                <br><br><br>
                      <h5 class="own-email" style="text-align: center;">Hotels In Pyin Oo Lwin</h5>
                <br><br>
                        <p class="email-info" style="text-align: center;padding-left:30px;">
                           ပြင်ဦးလွင်မြို့ရှိ Hotel များကို စုစည်းထားသည့် Wesite ကို iHost မှ တာဝန်ယူဆောင်ရွက်ပေးထားပါသည်။<br>
                           <a href="http://hotels.spidernetworkict.com/" target="blank_">http://hotels.spidernetworkict.com//</a>

                        </p>
                    </div>
               <!-- texts -->
               <div class="col-lg-6 col-md-6">
                  <img class="ipresponsive" src="images/hotel_responsive.png"> <br>
                 
               </div>
              </div>
         </div>
      </section>
      <section class="contact py-lg-4 py-md-3 py-sm-3 py-3" style="background-color: #f9f9f9;">
         <div class="container py-lg-5 py-md-5 py-sm-4 py-3">
            <div class="row">
               <div class="col-lg-6 col-md-6">
                  <img class="ipresponsive" src="images/food_responsive.png"> <br>
              </div>
               <!--//texts -->
                       <div class="col-lg-6 col-md-6">
                <br><br><br>
                      <h5 class="own-email" style="text-align: center;">Foods In Pyin Oo Lwin</h5>
                
                   <br><br>
                        <p class="email-info" style="text-align: center;padding-left:30px;">
                           ပြင်ဦးလွင်မြို့ရှိ အစားအသောက်များကို စုစည်းထားသည့် Wesite ကို iHost မှ တာဝန်ယူဆောင်ရွက်ပေးထားပါသည်။<br>
                           <a href="http://foods.spidernetworkict.com/" target="blank_">http://foods.spidernetworkict.com/</a>

                        </p>
                    </div>
               <!-- texts -->
            </div>
         </div>
      </section>
       <section class="contact py-lg-4 py-md-3 py-sm-3 py-3" style="background-color: #f9f9f9;">
         <div class="container py-lg-5 py-md-5 py-sm-4 py-3">
            <div class="row">
              <!--//texts -->
                        <div class="col-lg-6 col-md-6">
                <br><br><br>
                      <h5 class="own-email" style="text-align: center;">Wifi Management</h5>
                
                   <br><br>
                        <p class="email-info" style="text-align: center;padding-left:30px;">
                           Spider Network မှ လက်ရှိ အသုံးပြုနေသည့် Wifi Management Software ကို iHost မှ တာဝန်ယူဆောင်ရွက်ပေးထားပါသည်။<br>
                           <!-- <a href="http://wifi.spidernetworkict.com/" target="blank_">http://wifi.spidernetworkict.com/</a> -->
                        </p>
                    </div>
               <!-- texts -->
               <div class="col-lg-6 col-md-6">
                  <img class="ipresponsive" src="images/wifi_responsive.png"> <br>
                 
               </div>
              
            </div>
         </div>
      </section>
      <!--//contact -->
      
      @include('footer')
