<!DOCTYPE html>
<html lang="en">
   <head>
      <title>iHost : Web Hosting & Domain</title>
      <!--meta tags -->
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content="ihost,iHost,domain,hosting,web hosting,email,myanmar hosting,spider"/>
      <LINK rel="SHORTCUT ICON" href="images/ishortcut.png">
      <script>
         addEventListener("load", function () {
             setTimeout(hideURLbar, 0);
         }, false);
         
         function hideURLbar() {
             window.scrollTo(0, 1);
         }
      </script>
      <!--booststrap-->
      <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
      <!--//booststrap end-->
      <!-- font-awesome icons -->
      <link href="{{url('css/font-awesome.min.css')}}" rel="stylesheet">
      <!-- //font-awesome icons -->
      <!--stylesheets-->
      <link href="css/style.css" rel='stylesheet' type='text/css' media="all">
      <link href="css/responsive.css" rel='stylesheet' type='text/css' media="all">
      <!--//stylesheets-->
      <!-- Font Awesome -->
      <link rel="stylesheet" href="{{url('/css/font.css')}}">
      <link rel="stylesheet" href="{{url('/css/font2.css')}}">

      <!-- <link href="css/font.css" rel="stylesheet" type='text/css/' media="all">
      <link href="css/font2.css" rel="stylesheet" type='text/css/' media="all"> -->
   </head>
   <body>
      <div class="main-top" id="home">
         <!-- header -->
         <div class="headder-top d-lg-flex justify-content-between align-items-center py-3 px-sm-3">
            <!-- logo -->
            <div id="logo">
               <h1>
                  <a href="{{ route('index') }}">iHost</a>
               </h1>
            </div>
            <!-- //logo -->
            <!-- nav -->
            <nav>
               <label for="drop" class="toggle">Menu</label>
               <input type="checkbox" id="drop" />
               <ul class="menu">
                  <li>
                     <a href="{{ route('index') }}"  class="active">Web</a>
                  </li>
                  <li>
                     <a href="{{ route('domain') }}">Domain</a>
                  </li>
                  <li>
                     <a href="{{ route('hosting') }}">Hosting</a>
                  </li>
                  <li>
                     <a href="{{ route('email') }}">Email</a>
                  </li>
                  <li>
                  <a href="{{ route('contact') }}">Contact</a>
                  </li>
                  <li>
                  <a href="{{ route('project') }}">Our Projects</a>
                  </li>
               </ul>
            </nav>
            <!-- //nav -->
            <div class="d-flex mt-lg-1 mt-sm-2 mt-3 justify-content-center">
               <div class="search-w3layouts">
                  <!-- <span class="fa fa-users" aria-hidden="true"> -->
                  <button style="font-size:17px;padding: 8px 1px;">👩🏻‍💻 LOGIN</button>
                  </span>
                  <!-- modal form -->
                  <div class="popup">
                     <div class="content">
                        <span class="closebtn">&#9932;</span>
                        <div class="title">
                           <h1>Admin Login</h1>
                        </div>
                        <!-- <img src="https://webdevtrick.com/wp-content/uploads/logo-fb-1.png" alt="Car"> -->
                        <div class="subscribe">
                           <!-- <h1>Subscribe To Get The Notification Of Latest <span>POSTS</span></h1> -->
                           <form>
                              <input type="email" placeholder="Email Address">
                              <input type="password" placeholder="Password">
                              <input type="submit" value="Login">
                           </form>
                        </div>
                     </div>
                  </div>
                  <script src="js/jquery.min.js"></script>
                  <script src="js/modal.js"></script>
                  <!-- Modal login form -->
               </div>
            </div>
         </div>
      </div>
      <!-- //header -->