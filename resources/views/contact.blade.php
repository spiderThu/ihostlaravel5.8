<!DOCTYPE html>
<html lang="en">
   <head>
      <title>iHost : Web Hosting & Domain</title>
      <!--meta tags -->
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content="ihost,iHost,domain,hosting,web hosting,email,myanmar hosting,spider"/>
      <LINK rel="SHORTCUT ICON" href="images/ishortcut.png">
      <script>
         addEventListener("load", function () {
           setTimeout(hideURLbar, 0);
         }, false);
         
         function hideURLbar() {
           window.scrollTo(0, 1);
         }
      </script>
      <!--booststrap-->
      <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
      <!--//booststrap end-->
      <!-- font-awesome icons -->
      <link href="css/font-awesome.min.css" rel="stylesheet">
      <!-- //font-awesome icons -->
      <!--stylesheets-->
      <link href="css/style.css" rel='stylesheet' type='text/css' media="all">
      <link href="css/responsive.css" rel='stylesheet' type='text/css' media="all">
      <!--//stylesheets-->
      <link href="//fonts.googleapis.com/css?family=Cabin:400,500,600,700" rel="stylesheet">
      <link href="//fonts.googleapis.com/css?family=Roboto:300,400,500" rel="stylesheet">
   </head>
   <body>
    @if (Session::has('success'))
      <script type="text/javascript"> 
         alert("Your message had been send successfully!");
      </script>
    @endif
      <div class="inner_page-banner" id="home">
         <!-- header -->
         <div class="headder-top d-lg-flex justify-content-between align-items-center py-3 px-sm-3">
            <!-- logo -->
            <div id="logo">
               <h1>
                  <a href="{{ route('index') }}">iHost</a>
               </h1>
            </div>
            <!-- //logo -->
            <!-- nav -->
            <nav>
               <label for="drop" class="toggle">Menu</label>
               <input type="checkbox" id="drop" />
               <ul class="menu">
                  <li>
                     <a href="{{ route('index') }}">Web</a>
                  </li>
                  <li>
                     <a href="{{ route('domain') }}">Domain</a>
                  </li>
                  <li>
                     <a href="{{ route('hosting') }}">Hosting</a>
                  </li>
                  <li>
                     <a href="{{ route('email') }}">Email</a>
                  </li>
                  <li>
                     <a href="{{ route('contact') }}" class="active">Contact</a>
                  </li>
                  <li>
                     <a href="{{ route('project') }}">Our Projects</a>
                  </li>
               </ul>
            </nav>
            <!-- //nav -->
            <div class="d-flex mt-lg-1 mt-sm-2 mt-3 justify-content-center">
                    
                    <div class="search-w3layouts">
                        <!-- <span class="fa fa-users" aria-hidden="true"> -->
                            <button style="font-size:17px;padding: 8px 1px;">👩🏻‍💻 LOGIN</button>
                        </span>

                            <!-- modal form -->
                            <div class="popup">
                                <div class="content">

                                    <span class="closebtn">&#9932;</span>

                                    <div class="title">
                                        <h1>Admin Login</h1>
                                    </div>

                                    <!-- <img src="https://webdevtrick.com/wp-content/uploads/logo-fb-1.png" alt="Car"> -->

                                    <div class="subscribe">
                                        <!-- <h1>Subscribe To Get The Notification Of Latest <span>POSTS</span></h1> -->

                                        <form>
                                            <input type="email" placeholder="Email Address">
                                            <input type="password" placeholder="Password">
                                            <input type="submit" value="Login">
                                        </form>
                                    </div>

                                </div>
                            </div>
                            <script src='js/jquery.min.js'></script>
                            <script src="js/modal.js"></script>
                            <!-- Modal login form -->

                    </div>
                </div>
         </div>
         <div class="page-name teax-center">
            <h5>Contact Us</h5>
         </div>
      </div>
      <!-- //header -->
      <div class="using-border py-3">
         <div class="inner_breadcrumb  ml-4">
            <ul class="short_ls text-center">
               <li>
                  <a href="index.html">Home</a>
                  <span>/ /</span>
               </li>
               <li>Contact</li>
            </ul>
         </div>
      </div>
      <!-- contact -->
      <section class="contact py-lg-4 py-md-3 py-sm-3 py-3" style="background-color: #f9f9f9;">
         <div class="container py-lg-5 py-md-5 py-sm-4 py-3">
            <div class="row">
               <div class="col-lg-7 col-md-7">
                  <span class="contact-info">iHost သို့ဆက်သွယ်မေးမြန်းလိုပါက အောက်ပါအချက်အလက်များကို ဖြည့်စွက်ပြီးမေးမြန်းနိုင်ပါသည်။ iHost မှ့ 24 နာရီအတွင်း ပြန်လည်ဖြေကြားပေးပါမည်။</span>
                  <form action="{{url('send')}}" method="post" style="padding-top: 30px;">
                    @csrf
                     <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 form-group contact-forms">
                           <input type="text" name="first_name" class="form-control" placeholder="First Name" required="">
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 form-group contact-forms">
                           <input type="text" name="last_name" class="form-control" placeholder="Last Name" required="">
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 form-group contact-forms">
                           <input type="email" name="email" class="form-control" placeholder="Email"  required="">
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 form-group contact-forms">
                           <input type="text" name="phone" class="form-control" placeholder="Phone" required="">
                        </div>
                     </div>
                     <div class=" form-group contact-forms">
                        <textarea class="form-control" name="description" placeholder="Meassage" required=""></textarea>
                     </div>
                     <button type="submit" class="btn sent-butnn btn-lg">Send</button>
                  </form>
               </div>
               <!--//map -->
               <div class="col-lg-5 col-md-5 address_mail_footer_grids">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1849.347654943905!2d96.47792069497021!3d22.022982595538952!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30cc9ebd649c8579%3A0x8848d80c7b1858fc!2sSpider+Network+%26+CCTV!5e0!3m2!1sen!2smm!4v1562471151438!5m2!1sen!2smm" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                  <p class="pt-3" style="font-size: 14px;line-height: 2em; color: #9a9a9a;padding-left: 10px;">
                    ဆက်သွယ်ရန် :
                     <br>
                    အခန်း(၅) ၊ သစ်တောလမ်း ၊
                  တရုတ်ဘုံကျောင်းအနီး ၊ ပြင်ဦးလွင်မြို့။

                     <br>
                     Ph No. : 09-977870421 , 09-795938304
                  </p>
               </div>
               <!-- map -->
            </div>
         </div>
      </section>
      <!--//contact -->
      
      @include('footer')
