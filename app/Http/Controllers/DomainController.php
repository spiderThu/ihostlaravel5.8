<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;
use View;
use Mail;

class DomainController extends Controller
{
    public function searchDomain(Request $request)
	{
		$domain = $request->domain;   
        $godaddycheck = 'https://in.godaddy.com/domains/searchresults.aspx?checkAvail=1&tmskey=&domainToCheck='.$domain.'';
        $namecomcheck = 'https://www.name.com/domain/search/'.$domain.'';
        $registercomcheck = 'http://www.register.com/domain/search/wizard.rcmx?searchDomainName='.$domain.'&searchPath=Default&searchTlds=';
        if ( gethostbyname($domain) != $domain ) {
          $already = "already";
          return view('domain_results',compact('domain','already'));
       }
       else {
          $success = "success";
          return view('domain_results',compact('domain','success'));
       }
	}
}
