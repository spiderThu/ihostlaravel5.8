<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;
use View;
use Mail;

class SendController extends Controller
{
    public function send(Request $request)
	{

		$mailid = $request->email;
		$fname = $request->first_name;
		$lname = $request->last_name;
		$phone = $request->phone;
		$description = $request->description;
		$subject = 'iHost Customer Information';
	
		$data = array('email' => $mailid, 'subject' => $subject, 
					  'first_name' => $fname, 
					  'last_name' => $lname, 
					  'phone' => $phone, 
					  'description' => $description, 
					    );

		Mail::send('emails.newsinfo', $data , function ($message) use ($data) {
			$message->from('info@spidernetworkict.com', 'iHost Information');
			$message->to($data['email']);
			$message->subject($data['subject']); 
		});

		return redirect()->back()->with('success', ['Your message had been send successfully!']);   
	}
}
