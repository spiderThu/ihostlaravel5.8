<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('index');
    }

    public function domain()
    {
        return view('domain');
    }

    public function hosting()
    {
        return view('hosting');
    }

    public function email()
    {
        return view('email');
    }
    
    public function contact()
    {
        return view('contact');
    }

    public function project()
    {
        return view('project');
    }
    // public function logout () {
    //     //logout user
    //     auth()->logout();
    //     // redirect to homepage
    //     return redirect('/admin');
    // }
}
